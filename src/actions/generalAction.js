import  apis  from 'utils/apis.js';
import localStore from 'utils/localStorage';
import util from 'utils/util';

function handleNodeClick(qKey,node){
  return (dispatch, getState) => {
    dispatch({type:"NODE_DETAIL_SHOW",payload:{showDetail:true,key:qKey}});
    dispatch({type:"NODE_SELECTED",payload:{node:node,qKey:qKey}});
  }
}

function handleColorChange(qKey,oData){
  return{
    type: "NODE_COLOR_CHANGE",
    payload:{key:qKey,data:oData}
  }
}

function handleShowDetail(){
  return{
    type: "NODE_DETAIL_SHOW",
    payload:{showDetail:null}
  }
}

function graphItemSelect(item){
  return{
    type: "GRAPH_ITEM_SELECT",
    payload:{item:item}
  }
}

function handleConnect(postData){
  return (dispatch, getState) => {
    dispatch({type:"GEN_CHANGE_STATUS", payload:{"busy":true}});
    apis.createGraph(postData).then((data)=>{
      dispatch({type:"GEN_CHANGE_STATUS", payload:{"busy":false}});
      dispatch({type:"CREATE_NEW_GRAPH", payload:data});
    }).catch(()=>dispatch({type:"GEN_CHANGE_STATUS", payload:{"busy":false}}))
  };
}

function handleLabelDoubleClick(type, name){
  return (dispatch, getState) => {
    const list = getState().general.graphList.filter(item=>item.selected);
    if(list.length === 0)return;
    const selectKey = list[0].key;
    dispatch({type:"GEN_CHANGE_STATUS", payload:{"busy":true}});
    apis.lableDoubleClickQuery(selectKey,type, name).then((data)=>{
      dispatch({type:"GEN_CHANGE_STATUS", payload:{"busy":false}});
      dispatch({type:"UPDATE_GRAPH", payload:{data:data,name:name}});
    }).catch(()=>dispatch({type:"GEN_CHANGE_STATUS", payload:{"busy":false}}))
  };
}

function handleNodeDoubleClick(qKey,node){
  return (dispatch, getState) => {
    const list = getState().general.graphList.filter(item=>item.selected);
    if(list.length === 0)return;
    const selectKey = list[0].key;
    dispatch({type:"GEN_CHANGE_STATUS", payload:{"busy":true}});
    apis.nodeDoubleClick(selectKey,node).then((data)=>{
      const oQItem = list[0].queryList.filter(qItem=>qItem.key===qKey)[0];
      const nodes = util.andArrayNewDataNode(oQItem.graphData.nodes,data.graphData.nodes,oQItem.graphData.nodePro);
      const edges = util.andArrayNewDataEdge(oQItem.graphData.edges,data.graphData.edges,oQItem.graphData.edgePro);
      let graphData = {
        nodes:nodes.node,
        edges:edges.edge,
        nodePro:nodes.pro,
        edgePro:edges.pro,
      }
      dispatch({type:"GEN_CHANGE_STATUS", payload:{"busy":false}});
      dispatch({type:"UPDATE_GRAPH_BY_NODE_CLICK", payload:{data:{...data,graphData:graphData},qKey:qKey,node:node}});
    }).catch(()=>dispatch({type:"GEN_CHANGE_STATUS", payload:{"busy":false}}))
  };
}

function nodeQuery(str){
  return (dispatch, getState) => {
    const list = getState().general.graphList.filter(item=>item.selected);
    if(list.length === 0)return;
    const selectKey = list[0].key;
    dispatch({type:"GEN_CHANGE_STATUS", payload:{"busy":true}});
    apis.nodeQuery(selectKey,str).then((data)=>{
      dispatch({type:"GEN_CHANGE_STATUS", payload:{"busy":false}});
      dispatch({type:"UPDATE_GRAPH", payload:{data:data}});
    }).catch(()=>dispatch({type:"GEN_CHANGE_STATUS", payload:{"busy":false}}))
  };
}

function handleLabelClick(type, name){
  return (dispatch, getState) => {
    const list = getState().general.graphList.filter(item=>item.selected);
    if(list.length === 0)return;
    const selectKey = list[0].key;
    let bFetchPro = true;
    if(list[0].oNodeProperties[name]){
      bFetchPro = false;
    }
    dispatch({type:"GEN_CHANGE_STATUS", payload:{"busy":true}});
    apis.lableClickQuery(selectKey,type, name,bFetchPro).then((data)=>{
      dispatch({type:"GEN_CHANGE_STATUS", payload:{"busy":false}});
      dispatch({type:"UPDATE_GRAPH_MAIN_PANE", payload:data});
    }).catch(()=>dispatch({type:"GEN_CHANGE_STATUS", payload:{"busy":false}}))
  };
}

function getStoredGraph(){
  return (dispatch, getState) => {
    const store = localStore.get("graph") || {};
    for(let key in store){
      dispatch({type:"GEN_CHANGE_STATUS", payload:{"busy":true}});
      apis.getStoredGraph(key,store[key]).then((data)=>{
        dispatch({type:"CREATE_NEW_GRAPH", payload:data});
        dispatch({type:"GEN_CHANGE_STATUS", payload:{"busy":false}});
      })
    }

  };
}

function handleNFCancel(key){
  return{
    type: "NODE_CONFIGURE_SHOW",
    payload:{showNodeCofigure:false,key:key}
  }
}

function handleQueryPaneCollapset(key){
  return{
    type: "NODE_QUERY_PANE_SHOW",
    payload:{key:key}
  }
}

function handleQueryPaneRestore(key){
  return{
    type: "NODE_QUERY_PANE_RESTORE",
    payload:{key:key}
  }
}

function handleQueryPaneCloss(key){
  return{
    type: "NODE_QUERY_PANE_CLOSE",
    payload:{key:key}
  }
}

function handleCloseDetail(key){
  return{
    type: "NODE_DETAIL_SHOW",
    payload:{showDetail:false,key:key}
  }
}

function handleNodeLabelClick(key,name,bShowcf){
  return (dispatch, getState) => {
    const list = getState().general.graphList.filter(item=>item.selected);
    if(list.length === 0)return;
    const selectKey = list[0].key;
    const queryData = list[0].queryList.filter(item=>item.key === key)[0];
    if(queryData.nodeTypeProperties[name]){
      dispatch({type:"NODE_CONFIGURE_SHOW", payload:{showNodeCofigure:bShowcf,key:key,name:name}});
    }else{
      dispatch({type:"GEN_CHANGE_STATUS", payload:{"busy":true}});
      apis.getNodeProperties(selectKey,name).then((data)=>{
        dispatch({type:"NODE_CONFIGURE_SHOW", payload:{showNodeCofigure:bShowcf,key:key,name:name}});
        dispatch({type:"UPDATA_GTAPH_LIST_QUERY_PANE", payload:{data:data,Qkey:key,name:name}});
        dispatch({type:"GEN_CHANGE_STATUS", payload:{"busy":false}});
      })
    }
  };
}

function handleEdgeLabelClick(key,name,bShowcf){
  return (dispatch, getState) => {
    const list = getState().general.graphList.filter(item=>item.selected);
    if(list.length === 0)return;
    const selectKey = list[0].key;
    const queryData = list[0].queryList.filter(item=>item.key === key)[0];
    dispatch({type:"GEN_CHANGE_STATUS", payload:{"busy":true}});
    dispatch({type:"NODE_CONFIGURE_SHOW", payload:{showNodeCofigure:bShowcf,key:key,name:name}});
    dispatch({type:"UPDATA_GTAPH_LIST_QUERY_PANE", payload:{data:{nodeProperties:['label',"inVLabel","outVLabel"]},Qkey:key,name:name}});
    dispatch({type:"GEN_CHANGE_STATUS", payload:{"busy":false}});
  };
}

function deleteGraph(item){
  return (dispatch, getState) => {
    const list = getState().general.graphList.filter(item=>item.selected);
    if(list.length === 0)return;
    const selectKey = list[0].key;
    localStore.remove("graph",selectKey);
    dispatch({type:"GEN_CHANGE_STATUS", payload:{"busy":true}});
    dispatch({type:"DELETE_SELECTED_GRAPH", payload:{key:selectKey}});
    dispatch({type:"GEN_CHANGE_STATUS", payload:{"busy":false}});
  };
}

function changDisplayMode(qKey,sMode){
  return{
    type: "QUERY_PANE_DISPLAY_MODE_CHANGE",
    payload:{qKey:qKey,displayMode:sMode}
  }
}

export default {
  handleNodeClick,
  handleColorChange,
  handleShowDetail,
  graphItemSelect,
  handleConnect,
  nodeQuery,
  handleLabelClick,
  getStoredGraph,
  handleNFCancel,
  handleCloseDetail,
  handleNodeLabelClick,
  handleEdgeLabelClick,
  handleQueryPaneCollapset,
  handleLabelDoubleClick,
  handleNodeDoubleClick,
  handleQueryPaneCloss,
  deleteGraph,
  handleQueryPaneRestore,
  changDisplayMode
}
