/* eslint-disable */
import React from "react";
import { connect } from 'react-redux';
import PropTypes from "prop-types";
import { Switch, Route, Redirect } from "react-router-dom";
import { FormattedMessage } from 'react-intl';
// creates a beautiful scrollbar
import PerfectScrollbar from "perfect-scrollbar";
import "perfect-scrollbar/css/perfect-scrollbar.css";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import CircularProgress from '@material-ui/core/CircularProgress';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import IconButton from "@material-ui/core/IconButton";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Icon from '@material-ui/core/Icon';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Menu from "@material-ui/icons/Menu";
// core components
import Header from "components/Header/Header.jsx";
import Footer from "components/Footer/Footer.jsx";
import Sidebar from "components/Sidebar/Sidebar.jsx";
import Message from 'components/message/message';
import HomeList from 'views/layout/HomeList';
import AddNew from 'views/infoPane/addNew';
import DisplayGraphInfo from 'views/infoPane/displayGraphInfo';
import DialogDel from "components/deleteDlg/DialogDel.jsx";

import DetailInfo from 'views/infoPane/detailInfo';
import MainPane from 'views/mainPane/main';

import routeRules from "routes/route.jsx";

import dashboardStyle from "resources/style/layouts/dashboardStyle.jsx";

import generalAc from "actions/generalAction.js";

const switchRoutes = (
  <Switch>
    {routeRules.routePath.map((prop, key) => {
      if (prop.redirect)
        return <Redirect from={prop.path} to={prop.to} key={key} />;
      return <Route path={prop.path} component={prop.component} key={key} />;
    })}
  </Switch>
);

class App extends React.Component {
  constructor(props) {
    super(props);
    this.defaultWindowWidth = window.innerWidt;
    this.state = {
      createOpen: false,
      infoOpen:false,
      mainClass:"mainPanel",
      fullScreen: false,
      windowWidth:window.innerWidth,
      showDisplayGraphInfo:true,
      collapseStatus:{},
    };
    this.resizeFunction = this.resizeFunction.bind(this);
  }
  handleDrawerToggle = () => {
    this.setState({ mobileOpen: !this.state.mobileOpen });
    //  this.setState({mainClass:!this.state.mobileOpen?"mainPanel":"mainPanelFull"});
  };

  handleInfoDrawerToggle = () => {
    this.setState({ infoOpen: !this.state.infoOpen });
    // this.setState({mainClass:!this.state.mobileOpen?"mainPanel":"mainPanelFull"});
  };

  handlShowCollapse(name){
    let tmpCollapseStatus = {...this.state.collapseStatus};
    if(tmpCollapseStatus[name] == undefined){
      tmpCollapseStatus[name] = true;
    }else{
      tmpCollapseStatus[name] = !tmpCollapseStatus[name];
    }

    this.setState({ collapseStatus: tmpCollapseStatus});
  };

  resizeFunction() {
    this.setState({ windowWidth: window.innerWidth });
    // if (window.innerWidth >= 960) {
    //   this.setState({ mobileOpen: false });
    // }
  }
  componentDidMount() {

    if (navigator.platform.indexOf("Win") > -1) {
      const ps = new PerfectScrollbar(this.refs.mainPanel);
    }
    this.props.getStoredGraph();
    window.addEventListener("resize", this.resizeFunction);
  }
  componentDidUpdate(e) {
    if (e.history.location.pathname !== e.location.pathname) {
      this.refs.mainPanel.scrollTop = 0;
      if (this.state.mobileOpen) {
        this.setState({ mobileOpen: false });
      }
    }
  }
  componentWillUnmount() {
    window.removeEventListener("resize", this.resizeFunction);
  }

  itemSelect(item){
    if(item){
      if(item.selected){
        this.setState({ showDisplayGraphInfo: !this.state.showDisplayGraphInfo,
                        mainClass:this.state.showDisplayGraphInfo?"mainPanelFull":"mainPanel"});
      }
      this.props.itemSelect(item);
    }else{
      this.setState({ createOpen: true});
    }

  }
  componentWillReceiveProps(nextProps){
    if(nextProps.oSelectGraph && this.props.oSelectGraph && nextProps.oSelectGraph.key !== this.props.oSelectGraph.key){
      this.setState({ showDisplayGraphInfo: true,
                      mainClass:"mainPanel"});
    }
  }

  closeDlg(){
    this.setState({ createOpen: false});
  }

  clickConnect(data){
    if(this.props.handleConnect){
      this.props.handleConnect(data);
    }
    this.setState({ createOpen: false});
  }

  handleQueryPaneFullScreen(bShow){
    this.setState({
      fullScreen:bShow,
      mainClass:bShow?"mainClassFullScree": !this.state.showDisplayGraphInfo?"mainPanelFull":"mainPanel"
    })
  }
  // <IconButton
  //   className={classes.showDraver}
  //   color="inherit"
  //   aria-label="open drawer"
  //   onClick={this.handleDrawerToggle}
  // >
  //   <Menu />
  // </IconButton>
  render() {
    Message.defaultProps  = this.props;
    const { classes, ...rest } = this.props;
    return (
      <div className={classes.wrapper}>
        <HomeList
          listData={this.props.graphList}
          itemSelect={this.itemSelect.bind(this)}
          deleteGraph={this.props.deleteGraph}
          addSelect={this.props.addSelect}></HomeList>
        {
          this.props.selectGraphL.length > 0 ? (
            <DisplayGraphInfo
              handleDrawerToggle={this.handleDrawerToggle}
              collapseStatus={this.state.collapseStatus}
              handlShowCollapse={this.handlShowCollapse.bind(this)}
              open={this.state.showDisplayGraphInfo}
              nodePro={this.props.oSelectGraph.nodePro}
              edgePro={this.props.oSelectGraph.edgePro}
              nodeProperties={this.props.nodeProperties}
              handleLabelClick={this.props.handleLabelClick}
              handleLabelDoubleClick={this.props.handleLabelDoubleClick}
              connection={this.props.oSelectGraph.connection}
              color="blue"
              {...rest}
            />
        ) : null
        }
        <AddNew
          handleDrawerToggle={this.handleDrawerToggle}
          collapseStatus={this.state.collapseStatus}
          handlShowCollapse={this.handlShowCollapse.bind(this)}
          open={this.state.createOpen}
          color="blue"
          closeDlg={this.closeDlg.bind(this)}
          clickConnect={this.clickConnect.bind(this)}
          {...rest}
        />
      <div className={classes[this.state.mainClass]}  ref="mainPanel">
          <div className={classes.content}>
            <div className={classes.container}>
              <MainPane showFull={!this.state.showDisplayGraphInfo}
                newWidth={window.innerWidth}
                newHeight={window.innerHeight}
                handleQueryPaneFullScreen={this.handleQueryPaneFullScreen.bind(this)}
                ></MainPane>
            </div>
          </div>
        </div>
        {
          this.props.busy === true?(
            <div id="busy" className={classes.busyContent}>
              <CircularProgress className={classes.progress} size={150} />
            </div>
          ): null
        }
      </div>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = (state, ownProps) => {
  const oSelectGraph = state.general.graphList.filter(item=>item.selected)[0];
  const nodeProperties = oSelectGraph && oSelectGraph.oNodeProperties;
  return {
      busy: state.general.busy,
      MessageOpen: state.general.MessageOpen,
      message:state.general.message,
      msgType : state.general.msgType,
      selectedNode:state.general.selectedNode,
      showDetail:state.general.showDetail,
      graphList:state.general.graphList,
      selectGraphL: state.general.graphList.filter(item=>item.selected ? item : null),
      nodeProperties:nodeProperties,
      oSelectGraph: oSelectGraph || {},
      addSelect:state.general.addSelect
      // message:state.general.message
    }
};

const mapDispatchToProps = (dispatch)=>{

    return {
      handleMessageClose:()=>dispatch(generalAc.messageStateChange({MessageOpen:false})),
      handleShowDetail:()=>dispatch(generalAc.handleShowDetail()),
      handleColorChange:(color)=>dispatch(generalAc.handleColorChange(color)),
      itemSelect:(item)=>dispatch(generalAc.graphItemSelect(item)),
      handleConnect:(data)=>dispatch(generalAc.handleConnect(data)),
      handleLabelClick:(type,name)=>dispatch(generalAc.handleLabelClick(type,name)),
      handleLabelDoubleClick:(type,name)=>dispatch(generalAc.handleLabelDoubleClick(type,name)),
      getStoredGraph:()=>dispatch(generalAc.getStoredGraph()),
      deleteGraph:(item)=>dispatch(generalAc.deleteGraph(item))
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(withStyles(dashboardStyle)(App));
