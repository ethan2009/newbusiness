import { createStore, applyMiddleware } from "redux";
import rootReducer from 'reducers';
import ReduxThunk from "redux-thunk";

let store = createStore(rootReducer, applyMiddleware(ReduxThunk));

export default store;
