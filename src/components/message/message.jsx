import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Button from '@material-ui/core/Button';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ErrorIcon from '@material-ui/icons/Error';
import InfoIcon from '@material-ui/icons/Info';
import CloseIcon from '@material-ui/icons/Close';
import green from '@material-ui/core/colors/green';
import amber from '@material-ui/core/colors/amber';
import IconButton from '@material-ui/core/IconButton';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import WarningIcon from '@material-ui/icons/Warning';
import { withStyles } from '@material-ui/core/styles';
import MessageContent from 'components/message/messageContent';

const styles2 = theme => ({
  margin: {
    margin: theme.spacing.unit,
  },
});

class CustomizedSnackbars extends React.Component {

  render() {
    const { classes } = this.props;

    return (
      <Snackbar
         anchorOrigin={{
           vertical: 'top',
           horizontal: 'center',
         }}
         open={this.props.MessageOpen}
         autoHideDuration={3000}
         onClose={this.props.handleMessageClose}
       >
         <MessageContent
           onClose={this.props.handleMessageClose}
           variant={this.props.msgType}
           message={this.props.message}
         />
       </Snackbar>
    );
  }
}

CustomizedSnackbars.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles2)(CustomizedSnackbars);
