import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";
import { FormattedMessage } from 'react-intl';
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Drawer from "@material-ui/core/Drawer";
import Hidden from "@material-ui/core/Hidden";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Icon from "@material-ui/core/Icon";
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/icons/Menu";
import Collapse from "@material-ui/core/Collapse";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
// core components

import sidebarStyle from "resources/style/components/sidebarStyle.jsx";

const Sidebar = ({ ...props }) => {
  // verifies if routeName is the one active (in browser input)
  function activeRoute(routeName) {
    return props.location.pathname.indexOf(routeName) > -1 ? true : false;
  }

  function handlShowCollapseInner(stateName){
    handlShowCollapse(stateName);
  }

  let count = -1;

  function buildNavBar(aRoutes){
    count++;
    return (
      <List className={classes.list,classes["nested" + count]}>
        {aRoutes.map((prop, key) => {
          if (prop.redirect) return null;
          var activePro = " ";
          var listItemClasses;

          listItemClasses = classNames({
            [" " + classes[color]]: activeRoute(prop.path)
          });

          const whiteFontClasses = classNames({
            [" " + classes.whiteFont]: activeRoute(prop.path)
          });
          if(prop.children && prop.children.length > 0){
            const stateName = prop.state;
            return (
            <div className={activePro + classes.item}>
              <ListItem button className={classes.itemLink + listItemClasses} key={prop.state} id={prop.state}
                onClick={(e) => {handlShowCollapse(e.currentTarget.getAttribute("id"))}}>
                <ListItemIcon className={classes.itemIcon + whiteFontClasses}>
                  {collapseStatus[prop.state] ? <ExpandLess /> : <ExpandMore />}
                </ListItemIcon>
                <ListItemText
                  inset
                  primary={ <FormattedMessage id={prop.sidebarName} /> }
                  className={classes.itemText + whiteFontClasses}
                  disableTypography={true}
                />
              </ListItem>
              <Collapse
                in={collapseStatus[prop.state]}
                timeout="auto" unmountOnExit>
                {
                  buildNavBar(prop.children)
                }
              </Collapse>
            </div>
            );
          }else{
            return (
              <NavLink
                to={prop.path}
                className={activePro + classes.item}
                activeClassName="active"
                key={key}
              >
                <ListItem button className={classes.itemLink + listItemClasses}>
                  <ListItemIcon className={classes.itemIcon + whiteFontClasses}>
                    {typeof prop.icon === "string" ? (
                      <Icon>{prop.icon}</Icon>
                    ) : (
                      <prop.icon />
                    )}
                  </ListItemIcon>
                  <ListItemText
                    primary={ <FormattedMessage id={prop.sidebarName} /> }
                    className={classes.itemText + whiteFontClasses}
                    disableTypography={true}
                  />
                </ListItem>
              </NavLink>
            );
          }
        })}
      </List>
    );
  }

  const { classes, color, logo, image, logoText, routes, handlShowCollapse,collapseStatus} = props;
  var links = buildNavBar(routes);
  var brand = (
    <div className={classes.logo}>
      <a className={classes.logoLink}>

        <IconButton
          className={classes.img}
          color="inherit"
          aria-label="open drawer"
          onClick={props.handleDrawerToggle}
        >
          <Menu />
        </IconButton>

        {logoText}
      </a>
    </div>
  );
  return (
    <div>
      <Hidden mdUp implementation="css">
        <Drawer
          variant="temporary"
          anchor="right"
          open={false}
          classes={{
            paper: classes.drawerPaper
          }}
          onClose={props.handleDrawerToggle}
          ModalProps={{
            keepMounted: true // Better open performance on mobile.
          }}
        >
          {brand}
          {image !== undefined ? (
            <div
              className={classes.background}
              style={{ backgroundImage: "url(" + image + ")" }}
            />
          ) : null}
        </Drawer>
      </Hidden>
      <Hidden smDown implementation="css">
        <Drawer
          anchor="left"
          variant="permanent"
          open={props.open}
          classes={{
            paper: props.open?classes.drawerPaper:classes.drawerPaperHiden
          }}
          onClose={props.handleDrawerToggle}
          ModalProps={{
            keepMounted: true // Better open performance on mobile.
          }}
        >
          {brand}
          <div className={classes.sidebarWrapper}>{links}</div>
          {image !== undefined ? (
            <div
              className={classes.background}
              style={{ backgroundImage: "url(" + image + ")" }}
            />
          ) : null}
        </Drawer>
      </Hidden>
    </div>
  );
};

Sidebar.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(sidebarStyle)(Sidebar);
