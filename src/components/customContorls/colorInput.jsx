import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Text from "@material-ui/core/TextField";
import Input from "@material-ui/core/Input";
// @material-ui/icons
import Clear from "@material-ui/icons/Clear";
import Check from "@material-ui/icons/Check";
// core components
import customInputStyle from "resources/style/components/customInputStyle";

class ColorInput extends React.Component  {

  constructor(props) {
    super(props);
    this.value = "";
  }

  inputStrChange(e){
    // this.lastEditRange = window.getSelection().getRangeAt(0);\
    if(!this.doing){
      this.handleInput(e)
    }
  }

  handleInput(e){
    let sQueryStr = e.target.innerHTML;
    sQueryStr = sQueryStr.replace(/\n/g,"");
    sQueryStr = sQueryStr.replace(/<p[^\/<>]*>/g,"\n");
    sQueryStr = sQueryStr.replace(/<div[^\/<>]*>/g,"\n");

    sQueryStr = sQueryStr.replace(/<.[^<>]*>/g,"");
    if(this.props.onChange){
      this.props.onChange({
        target:{
          value:sQueryStr
        }
      })
    }
    this.value = sQueryStr;
    if(e.nativeEvent && e.nativeEvent.inputType === "insertFromPaste"){
      this.refs['input'].innerHTML = this.buildInputStr(sQueryStr);
    }

    this.refs['display'].innerHTML = this.buildDisplayStr(sQueryStr);
    // // this.refs['input'].innerHTML = "";
    // let child = document.createElement("p");
    // // child.style.margin = 0;
    // child.innerHTML = sHtml;
    // if(this.refs['input'].children[0]){
    //   this.refs['input'].removeChild(this.refs['input'].children[0])
    // }
    //
    // this.refs['input'].appendChild(child);
  }

  keyUp(e){
    if(e.ctrlKey  && e.keyCode == 13){
      e.preventDefault();
      if (this.props.onEndInput) {
        this.props.onEndInput();
      }
      this.refs['input'].innerHTML = "";
      return false;
    }
   // }else if(e.ctrlKey  && e.keyCode == 10){
   //   this.setState({
   //     queryStr:this.state.queryStr + "\n"
   //   })
   // }
  }


  componentDidMount() {
    document.getElementById("input").addEventListener('keyup',this.keyUp.bind(this));
    document.getElementById('input').addEventListener('compositionstart',(e)=>{
        this.doing=true;
    },false);

    document.getElementById('input').addEventListener('compositionend',(e)=>{
        this.doing=false;
        this.handleInput(e);
    },false);

    document.getElementById('input').addEventListener('scroll',(e)=>{
      this.refs['display'].scrollTop = this.refs['input'].scrollTop;
    },false);

  }

  componentDidUpdate(){
    if( this.props.value !== this.value && this.props.value !==""){
      let sHtml = this.buildDisplayStr(this.props.value);
      this.refs['display'].innerHTML = sHtml;
      let sHtmli = this.buildInputStr(this.props.value);
      this.refs['input'].innerHTML = sHtmli;
    }
    if(this.props.value === ""){
      this.refs['display'].innerHTML = "";
      this.refs['input'].innerHTML = "";
    }
    // let sHtml = this.buildInputStr(this.props.value);
    // this.refs['display'].innerHTML = sHtml;
    // let sHtml = this.buildInputStr(this.props.value);
    // // this.refs['input'].innerHTML = "";
    // let child = document.createElement("p");
    // // child.style.margin = 0;
    // child.innerHTML = sHtml;
    // if(this.refs['input'].children[0]){
    //   this.refs['input'].removeChild(this.refs['input'].children[0])
    // }
    //
    // this.refs['input'].appendChild(child);
    // if(window.getSelection().rangeCount > 0){
    //
    //   // if (this.lastEditRange) {
    //   //     window.getSelection().removeAllRanges()
    //   //     window.getSelection().addRange(this.lastEditRange)
    //   // }
    //
    //   let range = window.getSelection().getRangeAt(0);
    //   let textNode = range.startContainer;
    //   let rangeStartOffset = range.startOffset;
    //   // textNode.insertData(0, this.buildInputStr(this.props.value))\
    //   range.setStart(textNode,rangeStartOffset)
    //   // range.setStartAfter(child)
    //   range.collapse(true)
    //   window.getSelection().removeAllRanges()
    //   window.getSelection().addRange(range)
    //   this.lastEditRange = range
    // }
  }
  buildDisplayStr(str){
    const aKeyWords = this.props.keyWords.keywords;
    let rStr;
    let aStr =  str.replace(/\"[a-zA-Z][a-zA-Z]*\"|[a-zA-Z][a-zA-Z]*|[0-9][0-9]*/g,(str)=>{
      if(aKeyWords.indexOf(str) > -1){
        return '<font color="#FF6666">' + str + '</font>';
      }else if(/\"[a-zA-Z][a-zA-Z]*\"/.test(str)){
        return '<font color="#00cc00">' + str + '</font>';
      }else if(parseInt(str)){
        return '<font color="blue">' + str + '</font>';
      }else{
        return str;
      }
    }).split("\n");

    rStr = aStr[0];
    for(let i =1; i< aStr.length; i++){
      rStr = rStr + "<div>" + aStr[i] + "</div>";
    }
    return rStr.replace(new RegExp('<div></div>', "g" ),'<div><br></div>');
  }

  buildInputStr(str){
    const aKeyWords = this.props.keyWords.keywords;
    let rStr;
    let aStr =  str.split("\n");

    rStr = aStr[0];
    for(let i =1; i< aStr.length; i++){
      rStr = rStr + "<div>" + aStr[i] + "</div>";
    }
    return rStr.replace(new RegExp('<div></div>', "g" ),'<div><br></div>');
  }

  render(){
    const { classes } = this.props;
    return (
      <div className={classes.searchInput}>
        <div className={classes.searchInputField} id="input" contentEditable="true" tabindex="1" ref="input"
            onInput={this.inputStrChange.bind(this)}
          >
        </div>
        <div ref="display" className={classes.searchInputDisplay} style={{position:"absolute",top:0,height:"100%"}}>
        </div>
      </div>
    );
  }
}


export default withStyles(customInputStyle)(ColorInput);
