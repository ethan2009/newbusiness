import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import CancelIcon from '@material-ui/icons/Cancel';
import SaveIcon from '@material-ui/icons/Done';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from "@material-ui/core/TextField";
import Menu from '@material-ui/core/Menu';
import Search from "@material-ui/icons/Search";
import { FormattedMessage } from 'react-intl';
// core components
import tableStyle from "resources/style/components/tableStyle";

class CustomTable extends React.Component {
  constructor() {
    super();
    this.filterString = "";
    this.sortFlag = "";
    this.bChangeSelect = false;
  }

  state = {
    anchorEl: null,
    filterString:"",
    type:"S",
    property:"",
  };

  itemSelect(e){
    if(this.props.itemSelect)
    this.props.itemSelect(e.currentTarget.getAttribute("id"));
  };

  deleteFnc(row){
    if(this.props.deleteFn)
      this.props.deleteFn(row);
  };

  headerLineClick(property,event){
    if(property === this.state.property){
      this.setState({ anchorEl: event.currentTarget })
    }else{
      this.setState({ anchorEl: event.currentTarget, filterString:"" ,property:property})
      this.filterString  = "";
      this.sortFlag = "";
    }

  };

  filterChange(event){
    this.setState({ filterString:event.target.value});
  }

  handleClose = (property,type,event) => {
    this.bChangeSelect  = true;
    this.filterString = this.state.filterString;
    if(type === "S" ){
      this.setState({
        anchorEl: null,
      });
    }else{
      this.setState({
        anchorEl: null,
        type:type === "A" || type === "D" ?type:"S"
      });
    }

 };

 buildTableData = (tableData)=>{
   const proPath = this.props.propertyPath || "";
   let rData;
   if(!tableData || tableData.length === 0 )return tableData;
   if(this.state.type === "A" && this.state.property !== "" && proPath !== "" ){
     this.sortFlag = "A";
     rData = tableData.filter(item=>{  if(!item[proPath][this.state.property]) return item;
                                       if(item[proPath][this.state.property].indexOf(this.filterString) > -1) return item;
                                       return null})
              .sort((a,b)=>a[proPath][this.state.property] >= b[proPath][this.state.property] ? 1: -1)
   }else if(this.state.type === "D" && this.state.property !== ""){
     this.sortFlag = "D";
     rData = tableData.filter(item=>{  if(!item[proPath][this.state.property]) return item;
                                       if(item[proPath][this.state.property].indexOf(this.filterString) > -1) return item;
                                       return null})
              .sort((a,b)=>b[proPath][this.state.property] >= a[proPath][this.state.property] ? 1:-1)
   }else if(this.state.type === "S" && this.state.property !== ""){
     rData =  tableData.filter(item=>{  if(!item[proPath][this.state.property]) return item;
                                        if(item[proPath][this.state.property].indexOf(this.filterString) > -1) return item;
                                        return null});
     if(this.sortFlag === "A"){
       rData = rData.sort((a,b)=>a[proPath][this.state.property] >= b[proPath][this.state.property] ? 1: -1)
     }
     if(this.sortFlag === "D"){
       rData = rData.sort((a,b)=>b[proPath][this.state.property] >= a[proPath][this.state.property] ? 1: -1)
     }
   }else{
     rData =  tableData;
   }

   if(this.props.itemSelect && rData.length > 0 && this.bChangeSelect){
        this.props.itemSelect(rData[0][proPath][this.props.keyField]);
        this.bChangeSelect = false;
   }


   return rData;
 }

  render(){
    const { classes, tableHead, tableHeaderColor, bEdit, deleteFn, keyField,bExport ,filterEnable} = this.props;
    const tableData = this.buildTableData(this.props.tableData) || [];
    const bodyProperty = this.props.bodyProperty || [];
    const proPath = this.props.propertyPath || "";
    const noBodyProperty = bodyProperty.length === 0? true :false;
    const { anchorEl } = this.state;
    return (
        <Table className={classes.table}>
          {tableHead !== undefined ? (
            <TableHead className={classes[tableHeaderColor + "TableHeader"]}>
              <TableRow>
                {bEdit ? (  <TableCell className={classes.tableCell + " " + classes.tableHeadCell}>
                            </TableCell> ):null }
                {tableHead.map((prop, key) => {
                  return (
                    <TableCell
                      className={classes.tableCell + " " + classes.tableHeadCell}
                      key={key}
                    >
                      {<FormattedMessage id={prop}/>}
                      {
                        filterEnable && !noBodyProperty? (
                          <IconButton  aria-haspopup="true" aria-owns={anchorEl ? 'simple-menu' : null} className={classes.buttonHeaderline} onClick={this.headerLineClick.bind(this,bodyProperty[key])}>
                            <Icon className={classes.buttonHeaderlineIcon}>unfold_less</Icon>
                          </IconButton>

                        ) : null
                      }
                      <Menu
                           id="simple-menu"
                           anchorEl={anchorEl}
                           open={Boolean(anchorEl)}
                           onClose={this.handleClose.bind(this)}
                         >
                           <MenuItem className={classes.MenuItem}>
                             <FormattedMessage id="LABEL_SORT_DESC"/>
                             <IconButton  className={classes.menuItemButtom} onClick={this.handleClose.bind(this,bodyProperty[key],"D")}>
                               <Icon >arrow_downward</Icon>
                             </IconButton>
                           </MenuItem>
                           <MenuItem className={classes.MenuItem}>
                             <FormattedMessage id="LABEL_SORT_ASCE"/>
                             <IconButton  className={classes.menuItemButtom} onClick={this.handleClose.bind(this,bodyProperty[key],"A")}>
                               <Icon >arrow_upward</Icon>
                             </IconButton>
                           </MenuItem>
                           <MenuItem className={classes.MenuItem}>
                             <TextField
                                formControlProps={{
                                  fullWidth: true
                                }}
                                onChange={this.filterChange.bind(this)}
                                value={this.state.filterString}
                                className={classes.textField}
                                margin="normal" />
                              <IconButton  className={classes.menuItemButtomSearch} onClick={this.handleClose.bind(this,bodyProperty[key],"S")}>
                                <Search/>
                              </IconButton>
                           </MenuItem>
                      </Menu>
                    </TableCell>
                  );
                })}
              </TableRow>
            </TableHead>
          ) : null}
          <TableBody>
            {tableData.slice(this.props.page * this.props.rowsPerPage, this.props.page * this.props.rowsPerPage + this.props.rowsPerPage).map((prop, key) => {
              return (
                <TableRow key={key}
                  hover={true}
                  onClick={this.itemSelect.bind(this)}
                  id={prop[proPath] ? prop[proPath][keyField] : ""}
                  select={((prop[proPath] && prop[proPath][keyField] === this.props.selectedNo) || prop[keyField] === this.props.selectedNo) && this.props.mode !=="create"}
                  className={((prop[proPath] && prop[proPath][keyField] === this.props.selectedNo) || prop[keyField] === this.props.selectedNo) && this.props.mode !=="create"?classes.itemSelectstyles:null }
                >
                {bEdit ? (  <TableCell className={classes.tableCell}>
                              <IconButton id={prop[proPath] ? prop[proPath][keyField] : "123"} className={classes.button} onClick={this.deleteFnc.bind(this,prop)} aria-label="Delete">
                                <DeleteIcon />
                              </IconButton>
                            </TableCell> ):null }
                  {!noBodyProperty  ? bodyProperty.map((prop1, key) => {
                    return (
                      <TableCell className={classes.tableCell} key={key}>
                        {prop[proPath] ? (prop[proPath][prop1] || "") :prop[prop1]}
                      </TableCell>
                    );
                  }) : prop.map((prop1, key) => {
                    return (
                      <TableCell className={classes.tableCell} key={key}>
                        {prop1}
                      </TableCell>
                    );
                  }) }
                </TableRow>
              );
            })}
          </TableBody>
          <TableFooter>
            <TableRow>
              <TablePagination
                colSpan={12}
                count={tableData.length}
                rowsPerPage={this.props.rowsPerPage}
                rowsPerPageOptions={this.props.rowsPerPageOptions}
                page={this.props.page}
                onChangePage={this.props.handleChangePage}
                onChangeRowsPerPage={this.props.handleChangeRowsPerPage}
                labelRowsPerPage={<FormattedMessage id="LABEL_COUNT_PER_PAGE"/>}
                labelDisplayedRows={({from, to, count})=>from +" 到 "+to +" - 共 "+ count +" 条"}
              />
            </TableRow>
          </TableFooter>
        </Table>
    );
  }
}

CustomTable.defaultprops = {
  tableHeaderColor: "gray"
};

CustomTable.propTypes = {
  classes: PropTypes.object.isRequired,
  tableHeaderColor: PropTypes.oneOf([
    "warning",
    "primary",
    "danger",
    "success",
    "info",
    "rose",
    "gray"
  ]),
  tableHead: PropTypes.arrayOf(PropTypes.string),
  tableData: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string))
};

export default withStyles(tableStyle)(CustomTable);
