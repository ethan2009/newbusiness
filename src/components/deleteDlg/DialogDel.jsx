import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from "components/CustomButtons/Button.jsx";
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { FormattedMessage } from 'react-intl';

const styles={
  header:{
    backgroundColor:"#66bb6a",
    color:"#ffffff",
    padding:"0.5rem 1.5rem"
  },
  button:{
    height: "1rem",
    width: "1rem",
    lineHeight: "0.35rem",
  },
  content:{
    padding:" 0.5rem 1.5rem",
    fontSize: "2rem"
  }
};

class DeleteDialog extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  confirmDelete(event){
    if(this.props.confirmDelete){
      this.props.confirmDelete();
    }
  }

  render() {
    const { value,classes, ...other } = this.props;
    const products = this.props.products || [];
    return (
      <Dialog
        onClose={this.props.cancelDelete}
        open={this.props.open}
        aria-labelledby="confirmation-dialog-title"
        {...other}
      >
        <DialogContent className={classes.content}>
          <DialogContentText id="alert-dialog-description">
            <FormattedMessage id={this.props.text} />
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button  onClick={this.confirmDelete.bind(this)} color="transparent" className={classes.button}>
            <FormattedMessage id="BTN_CONFIRM"></FormattedMessage>
          </Button>
          <Button  onClick={this.props.cancelDelete} color="transparent" className={classes.button}>
            <FormattedMessage  id="BTN_CANCEL"></FormattedMessage>
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default withStyles(styles)(DeleteDialog);
