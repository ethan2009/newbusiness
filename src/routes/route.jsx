// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
import Person from "@material-ui/icons/Person";
import { FormattedMessage } from 'react-intl';
// import ContentPaste from "@material-ui/icons/ContentPaste";
import LibraryBooks from "@material-ui/icons/LibraryBooks";
import Customers from "@material-ui/icons/Face";
import BubbleChart from "@material-ui/icons/BubbleChart";
import GroupWork from "@material-ui/icons/GroupWork";
import LocationOn from "@material-ui/icons/LocationOn";
import Notifications from "@material-ui/icons/Notifications";
import Unarchive from "@material-ui/icons/Unarchive";
import AttachMoney from "@material-ui/icons/AttachMoney";
import Search from "@material-ui/icons/Search";
import Store from "@material-ui/icons/Store";

import Graph from "views/displayMode/Graph.jsx";

const routes = [
  // {
  //   path: "/home",
  //   sidebarName: "HOME_PAGE",
  //   icon: Dashboard,
  // },
  {
    path: "/home",
    sidebarName: "HOME_GRAPH",
    icon: Dashboard,
  },
  // {
  //   path: "/user",
  //   sidebarName: "USER_PROFILE",
  //   icon: Person,
  // },
  // {
  //   path: "#",
  //   sidebarName: "SALES_MODULE",
  //   icon: Store,
  //   state:"SALES_MODULE",
  //   children:[
  //     {
  //       path: "#",
  //       sidebarName: "REBATE_MANAGEMENT",
  //       icon: Person,
  //       state:"REBATE_MANAGEMENT",
  //       children:[
  //         {
  //           path: "/productGroup",
  //           sidebarName: "PRODUCT_GROUP",
  //           icon: GroupWork,
  //         },
  //         {
  //           path: "/rulesDefineMonthly",
  //           sidebarName: "RULE_DEDEFINE_M",
  //           icon: LibraryBooks,
  //         },
  //         {
  //           path: "/rulesDefineYearly",
  //           sidebarName: "RULE_DEDEFINE_Y",
  //           icon: LibraryBooks,
  //         },
  //         {
  //           path: "/customerAssign",
  //           sidebarName: "CUSTOMER_REBATE_ASSIGNMENT",
  //           icon: Customers,
  //         },
  //         {
  //           path: "/rebateCalMonthly",
  //           sidebarName: "REBATE_CAL_M",
  //           icon: AttachMoney,
  //         },
  //         {
  //           path: "/rebateCalYearly",
  //           sidebarName: "REBATE_CAL_Y",
  //           icon: AttachMoney,
  //         },
  //         {
  //           path: "/rebateBalance",
  //           sidebarName: "REBATE_BAL",
  //           icon: Search,
  //         },
  //       ]
  //     }
  //   ]
  // }
];

const routePath = [
  // {
  //   path: "/home",
  //   navbarName: "HOME_PAGE_DES",
  //   component: DashboardPage
  // },
  {
    path: "/home",
    navbarName: "HOME_GRAPH_DES",
    component: Graph
  },
  // {
  //   path: "/user",
  //   navbarName: "USER_PROFILE_DES",
  //   component: UserProfile
  // },
  // {
  //   path: "/productGroup",
  //   navbarName: "PRODUCT_GROUP_DES",
  //   component: ProductGroup
  // },
  // {
  //   path: "/rulesDefineMonthly",
  //   navbarName: "RULE_DEDEFINE_M_DES",
  //   component: RulesDefineM
  // },
  // {
  //   path: "/rulesDefineYearly",
  //   navbarName: "RULE_DEDEFINE_Y_DES",
  //   component: RulesDefineY
  // },
  // {
  //   path: "/customerAssign",
  //   navbarName: "CUSTOMER_REBATE_ASSIGNMENT_DES",
  //   component: CustomerAssign
  // },
  // {
  //   path: "/rebateCalMonthly",
  //   navbarName: "REBATE_CAL_M_DES",
  //   component: RebateCalM
  // },
  // {
  //   path: "/rebateCalYearly",
  //   navbarName: "REBATE_CAL_M_DES",
  //   component: RebateCalY
  // },
  // {
  //   path: "/rebateBalance",
  //   navbarName: "REBATE_BAL_DES",
  //   component: RebateBalance
  // },
  { redirect: true, path: "/", to: "/home", navbarName: "Redirect" }
]

export default {
  routePath,
  routes
}
