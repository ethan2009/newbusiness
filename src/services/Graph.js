import $ from 'jquery';

function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

function run_websocket_request(server_url, gremlin_query) {
    let msgData = {
        "requestId": uuidv4(),
        "op": "eval",
        "processor": "",
        "args": {
            "gremlin": gremlin_query,
            "bindings": {},
            "language": "gremlin-groovy"
        }
    };

    let msgJSONData = JSON.stringify(msgData);

    let ws_session = new WebSocket(server_url);

    ws_session.onopen = function (event) {
        ws_session.send(msgJSONData);
    };

    return new Promise((resolve, reject) => {

        ws_session.onerror = function (err) {
            console.log('Connection error using websocket');
            reject(err);
        };

        ws_session.onmessage = function (resultData) {
            let reponseData = JSON.parse(resultData.data);
            resolve(reponseData.result.data);

        };
    });
}

function run_ajax_request(server_url, gremlin_query) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            accept: "application/json",
            //contentType:"application/json; charset=utf-8",
            url: server_url,
            //headers: GRAPH_DATABASE_AUTH,
            // timeout: 2000,
            data: JSON.stringify({"gremlin": gremlin_query}),
            success: function (reponseData, textStatus, jqXHR) {
                var result = reponseData.result.data;
                resolve(result);
            },
            error: function (result, status, error) {
                reject("Connection failed. " + status);
            }
        });
    });
}

export default class Graph {
    connectInfo = {};

    constructor(graph_name, server_address, server_port, protocol, user, password) {
        this.connectInfo = {
            graph_name: graph_name,
            server_address: server_address,
            server_port: server_port,
            protocol: protocol,
            user: user,
            password: password
        }
    }

    get_nodes() {
        const gremlin_query_nodes = "g.V().groupCount().by(label).toList()";
        return new Promise((resolve, reject) => {
            this.execute_query(gremlin_query_nodes)
                .then((result) => resolve(result))
                .catch((err) => reject(err));
        });
    }

    get_edges() {
        const gremlin_query_edges = "g.E().groupCount().by(label).toList()";
        return new Promise((resolve, reject) => {
            this.execute_query(gremlin_query_edges)
                .then((result) => resolve(result))
                .catch((err) => reject(err));
        });
    }

    get_node_by_label(label = "*", limit = 100) {
        let label_query = "";

        if (label != "*") {
            label_query = ".hasLabel(\"" + label + "\")";
        }

        const gremlin_query_node = "nodes = g.V()" + label_query
            + ".limit(" + limit + ")" + '\n' + '[nodes.toList()]';

        return new Promise((resolve, reject) => {
            this.execute_query(gremlin_query_node)
                .then((result) => resolve(result))
                .catch((err) => reject(err));
        });
    }

    get_edge_by_label(label = "*", limit = 100) {

        let label_query = "";

        if (label != "*") {
            label_query = ".hasLabel(\"" + label + "\")";
        }

        const gremlin_query_edge = "edges = g.E()" + label_query + ".limit(" + limit + ").toList();"
            + "nodes = g.E(edges).bothV().dedup().toList();" + "[nodes,edges]";

        return new Promise((resolve, reject) => {
            this.execute_query(gremlin_query_edge)
                .then((result) => resolve(result))
                .catch((err) => reject(err));
        });

    }

    get_properties_by_label(label) {
        const gremlin_query_prop = "g.V().hasLabel(\"" + label
            + "\").limit(1).valueMap().select(keys).toList()";
        return new Promise((resolve, reject) => {
            this.execute_query(gremlin_query_prop)
                .then((result) => resolve(result))
                .catch((err) => reject(err));
        });
    }

    get_near_neighbour(id) {
        const gremlin_query_nodes = 'nodes = g.V(' + id
            + ').as("node").both().as("node").select(all,"node").inject(g.V(' + id + ')).unfold()';
        const gremlin_query_edges = "edges = g.V(" + id + ").bothE()";
        const gremlin_query_neighbour = gremlin_query_nodes + '\n' + gremlin_query_edges + '\n'
            + '[nodes.toList(),edges.toList()]';
        return new Promise((resolve, reject) => {
            this.execute_query(gremlin_query_neighbour)
                .then((result) => resolve(result))
                .catch((err) => reject(err));
        });
    }

    execute_query(gremlin_query) {
        let server_url;

        return new Promise((resolve, reject) => {
                if (this.connectInfo.protocol === "websocket") {
                    server_url = "ws://" + this.connectInfo.server_address + ":"
                        + this.connectInfo.server_port + "/gremlin";
                    run_websocket_request(server_url, gremlin_query)
                        .then((result) => resolve(result))
                        .catch((err) => reject(err));
                } else if (this.connectInfo.protocol === "REST") {
                    server_url = "http://" + this.connectInfo.server_address + ":" + this.connectInfo.server_port;
                    run_ajax_request(server_url, gremlin_query)
                        .then((result) => resolve(result))
                        .catch((err) => reject(err));
                } else {
                    console.log('Bad communication protocol. Check configuration file. Accept "REST" or "websocket" .');
                    reject("Bad communication protocol");
                }
            }
        );
    }

}
