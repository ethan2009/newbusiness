const initialState = {
  "selectedNode":{},
  "addSelect":true,
  "showDetail":false,
  "busy":false,
  "graphList":[]
};

export default function(state=initialState, action) {
  switch (action.type) {

    case "GEN_CHANGE_STATUS":{
      return {
        ...state,
        ...action.payload
      }
    }

    case "NODE_DETAIL_SHOW":{
      let key = action.payload.key;
      let aGraphListD = state.graphList.map((oItem) => {
        if(oItem.selected){
          const aQueryListD = oItem.queryList.map((oQItem)=>{
            return oQItem.key === key? {...oQItem,showDetail:action.payload.showDetail}:oQItem;
          })
          return { ...oItem, queryList: aQueryListD};
        }else{
          return oItem;
        }
      });

      return {
        ...state,
        graphList: aGraphListD
      }
    }

    case "NODE_QUERY_PANE_SHOW":{
      let key = action.payload.key;
      let aGraphListQP = state.graphList.map((oItem) => {
        if(oItem.selected){
          const aQueryListQP = oItem.queryList.map((oQItem)=>{
            return oQItem.key === key? {...oQItem,showCollapse:!oQItem.showCollapse}:oQItem;
          })
          return { ...oItem, queryList: aQueryListQP};
        }else{
          return oItem;
        }
      });

      return {
        ...state,
        graphList: aGraphListQP
      }
    }

    case "NODE_QUERY_PANE_CLOSE":{
      let key = action.payload.key;
      let aGraphListQP = state.graphList.map((oItem) => {
        if(oItem.selected){
          const aQueryListQP = oItem.queryList.filter((oQItem)=>{
            return oQItem.key !== key;
          })
          return { ...oItem, queryList: aQueryListQP};
        }else{
          return oItem;
        }
      });

      return {
        ...state,
        graphList: aGraphListQP
      }
    }

    case "NODE_CONFIGURE_SHOW":{
      let key = action.payload.key;
      let name = action.payload.name;
      let aGraphListC = state.graphList.map((oItem) => {
        if(oItem.selected){
          const aQueryListC = oItem.queryList.map((oQItem)=>{
            return oQItem.key === key? {...oQItem,showNodeCofigure:action.payload.showNodeCofigure,chooseLabel:name}:oQItem;
          })
          return { ...oItem, queryList: aQueryListC};
        }else{
          return oItem;
        }
      });

      return {
        ...state,
        graphList: aGraphListC
      }
    }

    case "NODE_SELECTED":{
      let node = action.payload.node;
      return {
        ...state,
        selectedNode: node
      }
    }

    case "QUERY_PANE_DISPLAY_MODE_CHANGE":{
      let key = action.payload.qKey;
      let displayMode = action.payload.displayMode;

      let cGraphList = state.graphList.map((item)=>{
        if(item.selected){
          let cQueryList = item.queryList.map((QItem)=>{
            if(key === QItem.key){
              return {...QItem,displayMode: displayMode};
            }else{
              return QItem;
            }
          });
          return {...item,queryList:cQueryList}
        }else{
          return item;
        }
      });

      return {
        ...state,
        graphList: cGraphList
      }
    }

    case "NODE_COLOR_CHANGE":{
      let key = action.payload.key;
      let oChangData = action.payload.data;
      let type = oChangData.openType;

      let cGraphList = state.graphList.map((item)=>{
        if(item.selected){
          let cQueryList = item.queryList.map((QItem)=>{
            if(key === QItem.key && type === "node"){
              let nodePro = {...QItem.graphData.nodePro};
              nodePro[QItem.chooseLabel].color = oChangData.color === ""?nodePro[QItem.chooseLabel].color:oChangData.color;
              nodePro[QItem.chooseLabel].size = oChangData.size === 0? nodePro[QItem.chooseLabel].size: oChangData.size;
              nodePro[QItem.chooseLabel].caption = oChangData.caption;
              return {...QItem,graphData:{...QItem.graphData,nodePro:nodePro},colorChnaged: !QItem.colorChnaged};
            }else if(key === QItem.key && type === "edge"){
              let edgePro = {...QItem.graphData.edgePro};
              edgePro[QItem.chooseLabel].color = oChangData.color === ""?edgePro[QItem.chooseLabel].color:oChangData.color;
              edgePro[QItem.chooseLabel].size = oChangData.size === 0? edgePro[QItem.chooseLabel].size: oChangData.size;
              edgePro[QItem.chooseLabel].caption = oChangData.caption;
              return {...QItem,graphData:{...QItem.graphData,edgePro:edgePro},colorChnaged: !QItem.colorChnaged};
            }else{
              return QItem;
            }
          });
          return {...item,queryList:cQueryList}
        }else{
          return item;
        }
      });

      return {
        ...state,
        graphList: cGraphList
      }
    }

    case "GRAPH_ITEM_SELECT":{
      const oItem = action.payload.item;
      let changed = true;
      let hasSelected = false;
      let selectedGraph = null;
      const graphList = state.graphList.map((item)=>{
        if(oItem && item.key == oItem.key){
          changed = item.selected ? false: true;
          item.selected = true;
          hasSelected = true;
        }else{
          item.selected = false;
        }
        return item;
      })
      if(!hasSelected && graphList[0]){
        graphList[0].selected = true;
      }
      if (changed) {
        return {
          ...state,
          graphList: graphList,
          selectedGraph: selectedGraph,
          addSelect: oItem? false: true
        }
      }else{
        return state;
      }
    }

    case "CREATE_NEW_GRAPH":{
      let graphList = state.graphList.map((oItem) => {return { ...oItem, selected: false}});
      let listItem = action.payload;
      // listItem.queryList.push({
      //   graphData:{
      //     nodes:[],
      //     edges:[]
      //   },
      //   showNodeCofigure:false,
      //   showDetail:false,
      //   showCollapse:false,
      //   selectedNode:{},
      //   nodePro:{...listItem.nodePro},
      //   edgePro:{...listItem.edgePro},
      //   showFullScreen:false,
      //   nodeTypeProperties:{},
      //   key:listItem.mainFnPaneKey
      // });
      graphList.unshift(listItem);
      return {
        ...state,
        graphList: graphList,
        addSelect: false
      }
    }

    case "UPDATE_GRAPH_MAIN_PANE":{
      const upMData = action.payload;
      const aQueryDataM = upMData.queryData;

      const aGraphListM = state.graphList.map((oItem) => {
        if(oItem.key === upMData.key){
          // const aQueryListM = oItem.queryList.map((oQItem)=>{
          //   const oNodeTypeProperties = {...oQItem.oNodeTypeProperties};
          //   if(upMData.nodeProperties){
          //     oNodeTypeProperties[upMData.nodeProperties.name] = upMData.nodeProperties.value;
          //   }
          //   return oQItem.key === oItem.mainFnPaneKey? {...oQItem, chooseLabel:upMData.nodeProperties.name, graphData:aQueryDataM.graphData, nodeTypeProperties:oNodeTypeProperties}:oQItem;
          // })
          const oNodeProperties = {...oItem.oNodeProperties};
          if(upMData.nodeProperties){
            oNodeProperties[upMData.nodeProperties.name] = upMData.nodeProperties.value;
          }
          return { ...oItem, oNodeProperties: oNodeProperties, selectLabel:upMData.nodeProperties.name};
        }
        return oItem;
      });

      return {
        ...state,
        graphList: aGraphListM
      }
    }

    case "UPDATA_GTAPH_LIST_QUERY_PANE":{
      const upQData = action.payload;
      const qKey = upQData.Qkey;
      const aGraphListQ = state.graphList.map((oItem) => {

        if(oItem.key === upQData.data.key){
          const aQueryListQ = oItem.queryList.map((oQItem)=>{
            const oNodeTypeProperties = {...oQItem.nodeTypeProperties};
            oNodeTypeProperties[upQData.data.nodeProperties.name] = upQData.data.nodeProperties.value;
            return oQItem.key === qKey? {...oQItem,nodeTypeProperties:oNodeTypeProperties,chooseLabel:upQData.name}:oQItem;
          })
          return { ...oItem, queryList: aQueryListQ}
        }
        return oItem;
      });

      return {
        ...state,
        graphList: aGraphListQ
      }
    }

    case "UPDATE_GRAPH":{
      const aupData = action.payload.data;
      const aqueryData = aupData.queryData;

      const agraphList = state.graphList.map((oItem) => {
        const aqueryList = [...oItem.queryList];
        if(oItem.key === aupData.key){
          let oProperties = {};
          if(aupData.nodeProperties){
            oProperties[aupData.nodeProperties.name] = aupData.nodeProperties.value;
          }
          aqueryData.showNodeCofigure =false;
          aqueryData.showDetail=false;
          aqueryData.showCollapse=false;
          aqueryData.selectedNode={};
          aqueryData.colorChnaged=false;
          aqueryData.doubleClickChaneg=false;
          aqueryData.chooseLabel=action.payload.name || "",
          aqueryData.showFullScreen=false;
          aqueryData.nodeTypeProperties=oProperties;
          aqueryList.unshift(aqueryData);
        }
        return oItem.key === aupData.key?{ ...oItem, queryList: aqueryList} : oItem;
      });

      return {
        ...state,
        graphList: agraphList
      }
    }

    case "NODE_QUERY_PANE_RESTORE":{
      let key = action.payload.key;
      let aNewGraphList = state.graphList.map((oItem) => {
        if(oItem.selected){
          const aQueryList = oItem.queryList.map((qItem)=>{
            if(qItem.key === key && qItem.graphData.origNodes && qItem.graphData.origEdes && qItem.graphData.origNodePro && qItem.graphData.origEdgePro){
              let graphData = {
                ...qItem.graphData,
                nodes:[...qItem.graphData.origNodes],
                edges:[...qItem.graphData.origEdes],
                nodePro:{...qItem.graphData.origNodePro},
                edgePro:{...qItem.graphData.origEdgePro}
              }
              return {...qItem,graphData:graphData,doubleClickChaneg: !qItem.doubleClickChaneg}
            }
            return qItem;
          })
          return { ...oItem, queryList: aQueryList}
        }
        return oItem;
      });

      return {
        ...state,
        graphList: aNewGraphList
      }
    }

    case "UPDATE_GRAPH_BY_NODE_CLICK": {
      let oNewData = action.payload.data;
      let key = action.payload.qKey;
      let node = action.payload.node ? action.payload.node : {...state.selectedNode};
      let aNewGraphList = state.graphList.map((oItem) => {
        if(oItem.key === oNewData.key){
          const aQueryList = oItem.queryList.map((qItem)=>{
            if(qItem.key === key){
              let graphData = {
                nodes:[...oNewData.graphData.nodes],
                edges:[...oNewData.graphData.edges],
                nodePro:{...oNewData.graphData.nodePro},
                edgePro:{...oNewData.graphData.edgePro},
                origNodes:[...qItem.graphData.nodes],
                origEdes:[...qItem.graphData.edges],
                origNodePro:{...qItem.graphData.nodePro},
                origEdgePro:{...qItem.graphData.edgePro}
              }
              return {...qItem,graphData:graphData,doubleClickChaneg: !qItem.doubleClickChaneg}
            }
            return qItem;
          })
          return { ...oItem, queryList: aQueryList}
        }
        return oItem;
      });

      return {
        ...state,
        selectedNode:node,
        graphList: aNewGraphList
      }
    }

    case "DELETE_SELECTED_GRAPH":{
      let key = action.payload.key;
      let iSelected = false;
      const newList = state.graphList.filter((item)=>{
        if(item.selected && item.key !== key){
          iSelected = item.selected;
        }
        return item.key !== key;
      });
      if(!iSelected && newList[0]){
        newList[0].selected = true ;
      }
      return {
        ...state,
        graphList:newList
      }
    }
    default:
      return state;
  }
}
