import { combineReducers } from 'redux';
import general from './generalReducer';

const allReducers = {
  general:general
}

const rootReducer = combineReducers(allReducers);

export default rootReducer;
