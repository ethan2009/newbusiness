import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch } from "react-router-dom";
import store from './store.js';
import { Provider } from 'react-redux';
import Translate from './resources/translate.js';
// import Graph from 'Graph.js';

// require("resources/style/containner.css");
import "./resources/style/css/base.css?v=1.4.1";

import indexRoutes from "./routes/index.jsx";

const hist = createBrowserHistory();

ReactDOM.render(

  <Translate Template={
    <Provider store={store}>
      <Router history={hist}>
        <Switch>
          {indexRoutes.map((prop, key) => {
            return <Route path={prop.path} component={prop.component} key={key} />;
          })}
        </Switch>
      </Router>
    </Provider>
  } />,
  document.getElementById("root")
);
