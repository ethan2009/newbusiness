import oData from "data.js";
import apiConfig from "utils/apiConfig";
import util from 'utils/util';
import localStore from 'utils/localStorage';
import 'whatwg-fetch';
import graphApi from 'services/Graph';
import constant from "utils/constant";

const generateKey = ()=>{
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

const getStoredGraph = (key,data)=>{
  const graphRequest = new graphApi(data.graph, data.host, data.port, "REST", data.username, data.password);
  const graphKey = key;
  apiConfig.CONNECTIONS[graphKey] = graphRequest;

  const aRequests = [];
  aRequests.push(graphRequest.get_nodes());
  aRequests.push(graphRequest.get_edges());
  // aRequests.push(graphRequest.get_node_by_label("*"));

  return new Promise((resolve,reject)=>{
    Promise.all(aRequests).then((aResults)=>{
      const nodes = util.parseProperty(aResults[0]);
      const edges = util.parseProperty(aResults[1]);
      // const dataMaps = util.parseData(allMaps);
      constant.buildDefaultNodeColors(nodes);
      constant.buildDefaultEdgeColors(edges);

      resolve({
        "name":data.graph,
        "key":graphKey,
        "selected":true,
        mainFnPaneKey:generateKey(),
        nodePro:nodes,
        edgePro:edges,
        oNodeProperties:{},
        queryList:[],
        connection:{
          host:data.host,
          port:data.port,
          protocol:"REST",
          username:data.username,
        }
      })
    }).catch(()=>{
      reject();
    });
  })
}

const createGraph = (data)=>{
  const graphRequest = new graphApi(data.graph, data.host, data.port, "REST", data.username, data.password);
  const graphKey = generateKey();
  apiConfig.CONNECTIONS[graphKey] = graphRequest;

  const aRequests = [];
  aRequests.push(graphRequest.get_nodes());
  aRequests.push(graphRequest.get_edges());
  // aRequests.push(graphRequest.get_node_by_label("*"));

  return new Promise((resolve,reject)=>{
    Promise.all(aRequests).then((aResults)=>{
      const nodes = util.parseProperty(aResults[0]);
      const edges = util.parseProperty(aResults[1]);
      constant.buildDefaultNodeColors(nodes);
      constant.buildDefaultEdgeColors(edges);
      // const dataMaps = util.parseData(allMaps);
      const store = localStore.get("graph") || {};
      store[graphKey] = data;
      localStore.set("graph",store);

      resolve({
        "name":data.graph,
        "key":graphKey,
        "selected":true,
        mainFnPaneKey:generateKey(),
        nodePro:nodes,
        selectLabel:"",
        edgePro:edges,
        oNodeProperties:{},
        queryList:[],
        connection:{
          host:data.graph,
          port:data.port,
          protocol:"REST",
          username:data.username,
        }
      })
    }).catch(()=>{
      reject();
    });
  })

  // return fetch( apiConfig.GET_API(apiConfig.APIS.CREATE), {
  //   method: 'POST',
  //   headers: {
  //     'Content-Type': 'application/json'
  //   },
  //   body: JSON.stringify(data)
  // }).then(res=>res.json());
}

const testConnect = (data)=>{
  return new Promise((resolve,reject)=>{
    resolve({
      "name":data.graph,
      "data":util.parseData(oData),
      "selected":false
    })
  })
}

const nodeQuery = (selectedKey,str)=>{
  return new Promise((resolve,reject)=>{
    apiConfig.CONNECTIONS[selectedKey].execute_query(str).then(data=>{
      let formatData = util.parseData(data);
      resolve({
        "key":selectedKey,
        queryData:{
          graphData:{
            nodes:formatData.nodes,
            edges:formatData.edges,
            nodePro:formatData.nodePro,
            edgePro:formatData.edgePro
          },
          QStr:str,
          key:generateKey()
        }
      });
    }).catch(error=>{
      reject()
    })
  })

}

const lableClickQuery = (selectKey,type,name,bFetchProperty)=>{

  return new Promise((resolve,reject)=>{
    apiConfig.CONNECTIONS[selectKey].get_properties_by_label(name).then((result)=>{
      let aValue = result["@value"][0]["@value"];
      aValue.unshift("id");
      resolve({
        "key":selectKey,
        nodeProperties: bFetchProperty ? {name:name,value:aValue}:null,
      })
    }).catch(()=>{
      reject();
    });
  })
}

const lableDoubleClickQuery = (selectKey,type,name)=>{
  const aRequests = [];

  aRequests.push(apiConfig.CONNECTIONS[selectKey].get_properties_by_label(name ==="All"?"*":name));
  if(type == "node"){
    aRequests.push(apiConfig.CONNECTIONS[selectKey].get_node_by_label(name ==="All"?"*":name));
  }else{
    aRequests.push(apiConfig.CONNECTIONS[selectKey].get_edge_by_label(name ==="All"?"*":name));
  }

  return new Promise((resolve,reject)=>{
    Promise.all(aRequests).then((aResults)=>{
      // const nodeData = util.parseQueryData(name,aResults[1]);
      let formatData = util.parseData(aResults[1]);
      let returnData = {
        "key":selectKey,
        queryData:{
          graphData:{
            nodes:formatData.nodes,
            edges:formatData.edges,
            nodePro:formatData.nodePro,
            edgePro:formatData.edgePro
          },
          key:generateKey()
        }
      };
      if(type == "node"){
        if(name !=="All"){
          let aValue = aResults[0]["@value"][0]["@value"];
          aValue.unshift("id");
          returnData.nodeProperties = {name:name,value:aValue};
        }
        returnData.queryData.QStr = "nodes = g.V().hasLabel(\"" + name + "\").limit(100)\n[nodes.toList()]";
      }else{
        returnData.queryData.QStr = "edges = g.E().hasLabel(\"" + name + "\").limit(100).toList();nodes = g.E(edges).bothV().dedup().toList();[nodes,edges]";
      }
      resolve(returnData)
    }).catch(()=>{
      reject();
    });
  })
}

const getNodeProperties = (selectKey,name)=>{

  return new Promise((resolve,reject)=>{
    apiConfig.CONNECTIONS[selectKey].get_properties_by_label(name).then((result)=>{
      let aValue = result["@value"][0]["@value"];
      aValue.unshift("id");
      let rData ={
        "key":selectKey,
        nodeProperties: {name:name,value:aValue},
      }
      resolve(rData)
    }).catch(()=>{
      reject();
    });
  })
}

const nodeDoubleClick = (selectedKy,node)=>{
  return new Promise((resolve,reject)=>{
    apiConfig.CONNECTIONS[selectedKy].get_near_neighbour(node.id).then((result)=>{
      let formatData = util.parseData(result);
      resolve({
        "key":selectedKy,
        graphData:{
          nodes:formatData.nodes,
          edges:formatData.edges,
          nodePro:formatData.nodePro,
          edgePro:formatData.edgePro
        }
      })
    }).catch(()=>{
      reject();
    });
  })
}

export default {
  createGraph,
  testConnect,
  nodeQuery,
  lableClickQuery,
  lableDoubleClickQuery,
  getStoredGraph,
  getNodeProperties,
  nodeDoubleClick
}
