const keywords = [
    'V',
    'addE',
    'addV',
    'aggregate',
    'and',
    'as',
    'barrier',
    'both',
    'bothE',
    'bothV',
    'branch',
    'cap',
    'choose',
    'coalesce',
    'coin',
    'constant',
    'count',
    'cyclicPath',
    'dedup',
    'drop',
    'emit',
    'filter',
    'flatMap',
    'fold',
    'group',
    'groupCount',
    'has',
    'hasId',
    'hasKey',
    'hasLabel',
    'hasNot',
    'hasValue',
    'id',
    'identity',
    'in',
    'inE',
    'inV',
    'index',
    'inject',
    'is',
    'key',
    'label',
    'limit',
    'local',
    'loops',
    'map',
    'match',
    'math',
    'max',
    'mean',
    'min',
    'not',
    'optional',
    'or',
    'order',
    'otherV',
    'out',
    'outE',
    'outV',
    'path',
    'project',
    'properties',
    'property',
    'propertyMap',
    'range',
    'repeat',
    'sack',
    'sample',
    'select',
    'sideEffect',
    'simplePath',
    'skip',
    'store',
    'subgraph',
    'sum',
    'tail',
    'timeLimit',
    'times',
    'to',
    'toE',
    'toV',
    'tree',
    'unfold',
    'union',
    'until',
    'value',
    'valueMap',
    'values',
    'where',
];

export default {keywords}
