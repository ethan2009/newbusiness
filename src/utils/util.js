import constant from "utils/constant";

const deepClone = (obj) => {
    if (!obj) {
        return null;
    }
    return JSON.parse(JSON.stringify(obj));
}

const getValue = (oData)=>{
  if(oData["@value"]["@value"]){
    return getValue(oData["@value"])
  }else if(oData["@value"].value && oData["@value"].value.hasOwnProperty && oData["@value"].value.hasOwnProperty("@value")){
    return getValue(oData["@value"].value)
  }else{
    let sValue = oData["@value"].value || oData["@value"].toString();
    return sValue.toString();
  }
}

const parseProperty = (data)=>{
  const aTmp = data["@value"][0]["@value"];
  const oPro = {All:{
    color: "#" + ("00000" + ((Math.random() * 16777215 + 0.5) >> 0).toString(16)).slice(-6),
    count: 0,
  }};
  for(let i = 0; i < aTmp.length; i+=2){
    oPro[aTmp[i]] = {
      color: "#" + ("00000" + ((Math.random() * 16777215 + 0.5) >> 0).toString(16)).slice(-6),
      count: aTmp[i+1]["@value"],
      caption:"id",
      size:1
    }
    oPro.All.count += aTmp[i+1]["@value"];
  }
  return oPro;
}

const parseQueryData = (name,data)=>{
  let nodeData = data && data["@value"].length > 0 && data["@value"][0]["@value"];
  let aData = [];
  for (let i = 0; i < nodeData.length; i++) {
    aData.push({
        id: nodeData[i]["@value"].id["@value"],
        name: name,
        nodeData: nodeData[i]["@value"].properties
    });
  }

  return aData;
}


const parseData = (data) => {
    let nodeData = data && data["@value"].length > 0 && data["@value"][0]["@value"];
    let relationData = data && data["@value"].length > 1 && data["@value"][1]["@value"] || [];
    let nodes = [];
    let edges = [];
    let nodePro = {};
    let edgePro = {};

    for (let i = 0; i < nodeData.length; i++) {

        nodes.push({
            id: nodeData[i]["@value"].id["@value"],
            name: nodeData[i]["@value"].label,
            nodeData: nodeData[i]["@value"].properties
        });

        if (!nodePro[nodeData[i]["@value"].label]) {
            nodePro[nodeData[i]["@value"].label] = {
                color: constant.defaultNodeColors[nodeData[i]["@value"].label],
                count: 0,
                caption:"id",
                size:1
            }
        }

        nodePro[nodeData[i]["@value"].label].count++;

    }

    for (let i = 0; i < relationData.length; i++) {

        edges.push({
            id: relationData[i]["@value"].id["@value"],
            target: relationData[i]["@value"].inV["@value"],
            source: relationData[i]["@value"].outV["@value"],
            tag: relationData[i]["@value"].label,
        });

        if (!edgePro[relationData[i]["@value"].label]) {
            edgePro[relationData[i]["@value"].label] = {
                color: constant.defaultEdgeColors[relationData[i]["@value"].label],
                count: 0,
                caption:"display",
                size:2
            }
        }

        edgePro[relationData[i]["@value"].label].count++;

    }

    return {
        nodes: nodes,
        nodePro: nodePro,
        edges: edges,
        edgePro: edgePro
    }

}

const andArrayNewDataNode = (aOld,aNew, oNodePro)=>{
  const aData = [...aOld];
  const oNPro = {...oNodePro};
  for(let i = 0; i < aNew.length; i++){
    let bExist = false;
    for(let j = 0; j < aData.length; j++){
      if(aNew[i].id === aData[j].id && aNew[i].name === aData[j].name){
        bExist = true;
        break;
      }
    }

    if(!bExist){
      aData.push(aNew[i]);
      if (!oNPro[aNew[i].name]) {
          oNPro[aNew[i].name] = {
              color: constant.defaultNodeColors[aNew[i].name],
              count: 0,
              caption:"id",
              size:1
          }
      }
      oNPro[aNew[i].name].count++;
    }
  }

  return {node:aData,pro:oNPro};
}

const andArrayNewDataEdge = (aOld,aNew,oEdgePro)=>{
  const aData = [...aOld];
  const oEPro = {...oEdgePro};
  for(let i = 0; i < aNew.length; i++){
    let bExist = false;
    for(let j = 0; j < aData.length; j++){
      if(aNew[i].target === aData[j].target && aNew[i].source === aData[j].source && aNew[i].tag === aData[j].tag){
        bExist = true;
        break;
      }
    }

    if(!bExist){
      aData.push(aNew[i]);
      if (!oEPro[aNew[i].tag]) {
          oEPro[aNew[i].tag] = {
              color: constant.defaultEdgeColors[aNew[i].tag],
              count: 0,
              caption:"display",
              size:2
          }
      }
      oEPro[aNew[i].tag].count++;

    }
  }

  return {edge:aData,pro:oEPro};
}

const addObjectNewData = (oOld,oNew)=>{
  const oData = {...oOld};
  for(let pro in oNew){
    if(!oData[pro]){
      oData[pro] = oNew[pro]
    }
  }

  return oData;
}

export default {
    deepClone,
    parseData,
    getValue,
    parseProperty,
    parseQueryData,
    andArrayNewDataEdge,
    andArrayNewDataNode,
    addObjectNewData
}
