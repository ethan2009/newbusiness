const set = (key,value)=>{
  if(typeof value === "object"){
    localStorage.setItem(key, JSON.stringify(value))
  }else{
    localStorage.setItem(key, JSON.stringify(value))
  }

}

const get = (key)=>{
  let value = localStorage.getItem(key);
  let reValue;
  try {
    reValue = JSON.parse(value);
  } catch (e) {
    reValue = {};
  }

  return reValue;
}

const remove = (key,value)=>{
  let oValue = localStorage.getItem(key);
  let reValue;
  try {
    reValue = JSON.parse(oValue);
    delete reValue[value];
    localStorage.setItem(key, JSON.stringify(reValue))
  } catch (e) {
  }
}


export default {
  set,
  get,
  remove
}
