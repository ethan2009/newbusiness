// const colors = ["#FF6666","#FFCCCC","#CCCCFF","#CCCCCC","#CCFF99","#FFCC99","#6699CC","#666699"]
 // "#009688", "#4caf50", "#8bc34a", "#cddc39",
 // "#ffeb3b", "#ffc107", "#ff9800", "#ff5722",
 // "#795548", "#607d8b",
 // '#4D4D4D', '#999999', '#FFFFFF', '#F44E3B',
 // '#FE9200', '#FCDC00', '#DBDF00', '#A4DD00',
 // '#68CCCA', '#73D8FF', '#AEA1FF', '#FDA1FF',
 // '#333333', '#808080', '#cccccc', '#D33115',
 // '#E27300', '#FCC400', '#B0BC00', '#68BC00',
 // '#16A5A5', '#009CE0', '#7B64FF', '#FA28FF',
 // '#000000', '#666666', '#B3B3B3', '#9F0500',
 // '#C45100', '#FB9E00', '#808900', '#194D33',
 // '#0C797D', '#0062B1', '#653294', '#AB149E']

// const colors1 = ["#FF6666","#FFCCCC","#CCCCFF","#CCCCCC","#CCFF99","#FFCC99","#6699CC","#666699",
//  "#CC9999","#66CCCC","#0099CC","#99CC00","#FF9966","#FFFFCC",
//  "#ABCDEF","#99CCFF","#336699","#CCCC44","#FF0033","#CC99CC","#FFFF99",
//  "#CCCC00","#CCFFFF","#99CCCC","#FF99CC","#FFCC00",'#C45100', '#FB9E00', '#808900', '#194D33']

const defaultColors =  {
    '#FFE081':{
        'borderColor': '#9AA1AC',
        'textColorInt': '#604A0E'
    },
    '#C990C0':{
        'borderColor': '#b261a5',
        'textColorInt': '#FFFFFF'
    },
    '#F79767':{
        'borderColor': '#f36924',
        'textColorInt': '#FFFFFF'
    },
    '#57C7E3':{
        'borderColor': '#23b3d7',
        'textColorInt': '#FFFFFF'
    },
    '#F16667':{
        'borderColor': '#eb2728',
        'textColorInt': '#FFFFFF'
    },
    '#D9C8AE':{
        'borderColor': '#c0a378',
        'textColorInt': '#604A0E'
    },
    '#8DCC93':{
        'borderColor': '#5db665',
        'textColorInt': '#604A0E'
    },
    '#ECB5C9':{
        'borderColor': '#da7298',
        'textColorInt': '#604A0E'
    },
    '#4C8EDA':{
        'borderColor': '#2870c2',
        'textColorInt': '#FFFFFF'
    },
    '#FFC454':{
        'borderColor': '#d7a013',
        'textColorInt': '#604A0E'
    },
    '#DA7194':{
        'borderColor': '#cc3c6c',
        'textColorInt': '#FFFFFF'
    },
    '#569480':{
        'borderColor': '#447666',
        'textColorInt': '#FFFFFF'
    }
}

const colors1 = Object.keys(defaultColors)

const colors = Object.keys(defaultColors)

const sStroke = [
  0.5,1,1.25,1.5,1.75,2
]

const sLineStroke = [
  1,2,4,8,10,16
]


const defaultNodeColors = {

}

const defaultEdgeColors = {

}

const buildDefaultNodeColors = (labels)=>{
  let aLabels = Object.keys(labels);
  for(let i = 0;i< aLabels.length; i++){
    const tmpColor = colors1.filter((item)=>{
      for(let pro in defaultNodeColors){
        if(defaultNodeColors[pro] === item)
        return null
      }
      return item;
    })
    defaultNodeColors[aLabels[i]] = tmpColor[Math.floor(Math.random()*tmpColor.length)] || colors1[Math.floor(Math.random()*colors1.length)]
  }
}

const buildDefaultEdgeColors = (labels)=>{
  let aLabels = Object.keys(labels);
  for(let i = 0;i< aLabels.length; i++){
    const tmpColor = colors1.filter((item)=>{
      for(let pro in defaultEdgeColors){
        if(defaultEdgeColors[pro] === item)
        return null
      }
      return item;
    })
    defaultEdgeColors[aLabels[i]] = tmpColor[Math.floor(Math.random()*tmpColor.length)] || colors1[Math.floor(Math.random()*colors1.length)]
  }
}



export default {
  colors,
  sStroke,
  sLineStroke,
  defaultColors,
  defaultNodeColors,
  defaultEdgeColors,
  buildDefaultNodeColors,
  buildDefaultEdgeColors
}
