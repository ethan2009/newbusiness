const SERVER_HOST = "http://www.wiseeds.com:3000/api/";

const APIS = {
  "CREATE":"create"
};

const CONNECTIONS = {

};

const GET_API = (path)=>{
  if(window.location.host.indexOf("localhost") > -1){
    return `${SERVER_HOST}`+ path;
  }else{
    return "/" + path;
  }
}

export default {
  APIS,
  GET_API,
  CONNECTIONS
}
