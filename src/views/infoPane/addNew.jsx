import React from "react";
import { connect } from 'react-redux';

// @material-ui/core components
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardAvatar from "components/Card/CardAvatar.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import withStyles from "@material-ui/core/styles/withStyles";
import InputLabel from "@material-ui/core/InputLabel";
import Drawer from "@material-ui/core/Drawer";
import Paper from '@material-ui/core/Paper';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import Typography from '@material-ui/core/Typography';
import DialogDel from "components/deleteDlg/DialogDel.jsx";
import Button from "components/CustomButtons/Button.jsx";

import { FormattedMessage } from 'react-intl';

import styles from "resources/style/components/sidebarStyle.jsx";

class AddNew extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      setSelecter: false,
      deleteOpen:false
    };
    this.postData = {};
  }

  onConnect(e){
    if (this.props.clickConnect) {
      this.props.clickConnect(this.postData)
      this.postData = {};
    }
    this.setState({
      deleteOpen: false
    })
  }

  cancelDelete(){
    this.setState({
      deleteOpen: false
    })
  }

  openDeleteDlg(e){
    if(Object.keys(this.postData).length === 0){
      this.props.closeDlg();
      this.postData = {};
    }else{
      this.setState({
        deleteOpen: true
      });
    }
  }

  handelValueChange(pro,e){
    this.postData[pro] =  e.target.value;
    if(e.target.value === ""){
      delete this.postData[pro];
    }
  }

  // onConnect(){
  //   if (this.props.clickConnect) {
  //     this.props.clickConnect(this.postData)
  //   }
  // }

  render(){
    const { classes } = this.props;
    return (
      <div>
      <Dialog
        onClose={this.openDeleteDlg.bind(this)}
        open={this.props.open}
        classes={{paperWidthSm:classes.paperWidthSm}}
        aria-labelledby="confirmation-dialog-title"
      >
        <DialogTitle disableTypography={true} id="confirmation-dialog-title" className={classes.dlgHeader}><FormattedMessage id="TIL_ADD_NEW_GRAPH" /></DialogTitle>
        <DialogContent className={classes.dlgContent}>
          <Grid container className={classes.addContainer}>
            <Grid item xs={12} sm={12} md={12} className={classes.addContainerItemInfo}>
              <span><FormattedMessage id="TEXT_ADD_NEW_GRAPH" /></span>
            </Grid>
            <Grid item xs={12} sm={12} md={12} className={classes.addContainerItem}>
              <TextField
                label="Graph Name"
                id="graph"
                margin="dense"
                style={{width:"100%"}}
                onChange={this.handelValueChange.bind(this,"graph")}
                formControlProps={{
                  fullWidth: true
                }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12} className={classes.addContainerItem}>
              <TextField
                label="Host"
                id="host"
                margin="dense"
                style={{width:"100%"}}
                onChange={this.handelValueChange.bind(this,"host")}
                formControlProps={{
                  fullWidth: true
                }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12} className={classes.addContainerItem}>
              <TextField
                label="Port"
                id="port"
                margin="dense"
                style={{width:"100%"}}
                onChange={this.handelValueChange.bind(this,"port")}
                formControlProps={{
                  fullWidth: true
                }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12} className={classes.addContainerItem}>
              <TextField
                label="Protocol"
                id="protocol"
                margin="dense"
                style={{width:"100%"}}
                value="REST"
                formControlProps={{
                  fullWidth: true
                }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12} className={classes.addContainerItem}>
              <TextField
                label="User Name"
                id="username"
                style={{width:"100%"}}
                margin="dense"
                onChange={this.handelValueChange.bind(this,"username")}
                formControlProps={{
                  fullWidth: true
                }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12} className={classes.addContainerItem}>
              <TextField
                label="Pass Word"
                id="password"
                style={{width:"100%"}}
                margin="dense"
                type={"password"}
                onChange={this.handelValueChange.bind(this,"password")}
                formControlProps={{
                  fullWidth: true
                }}
              />
            </Grid>
            </Grid>
        </DialogContent>
        <DialogActions className={classes.dlgActiongs}>
          <Button  onClick={this.onConnect.bind(this)} color="transparent" className={classes.button1}>
            <FormattedMessage id="BTN_CONNECT"></FormattedMessage>
          </Button>
          <Button  onClick={this.openDeleteDlg.bind(this)} color="transparent" className={classes.button1}>
            <FormattedMessage  id="BTN_CANCEL"></FormattedMessage>
          </Button>
        </DialogActions>
      </Dialog>
      <DialogDel
        confirmDelete={this.onConnect.bind(this)}
        cancelDelete={this.cancelDelete.bind(this)}
        text={"TEXT_CONFIRM_CLOSE_ADD"}
        open={this.state.deleteOpen}></DialogDel>
    </div>
    );
  }

}
// <Drawer
//   anchor="left"
//   variant="permanent"
//   open={true}
//   classes={{
//      paper: classes.drawerPaper//this.props.open?classes.drawerPaper:classes.drawerPaperHiden
//   }}
//   onClose={this.props.handleDrawerToggle}
//   ModalProps={{
//     keepMounted: true // Better open performance on mobile.
//   }}
// >
//   <div className={classes.sidebarWrapper}>
//     <Paper className={classes.addContainerHeader} elevation={1}>
//      <Typography variant="h5" component="h3">
//       Add New Graph
//      </Typography>
//    </Paper>
//
//       <Button color="blue" disableRipple className={classes.addContainerButton} onClick={this.onConnect.bind(this)}><FormattedMessage id='BTN_CONNECT'/></Button>
//   </div>
// </Drawer>

const mapStateToProps = (state, ownProps) => {
  return {
    }
};

const mapDispatchToProps = (dispatch)=>{
    return {
    }
}

// const MuiDialog = connect(mapStateToProps,mapDispatchToProps)(AddNew);

export default withStyles(styles)(connect(mapStateToProps,mapDispatchToProps)(AddNew));
