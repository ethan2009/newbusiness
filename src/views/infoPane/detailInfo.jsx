import React from "react";
import { connect } from 'react-redux';
import ColorPicker from 'react-color-picker';
import ss from 'react-color-picker/index.css';
import { SketchPicker,MaterialPicker   } from 'react-color';
// @material-ui/core components
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardAvatar from "components/Card/CardAvatar.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import withStyles from "@material-ui/core/styles/withStyles";
import InputLabel from "@material-ui/core/InputLabel";
import Drawer from "@material-ui/core/Drawer";
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import Button from "components/CustomButtons/Button.jsx";

import { FormattedMessage } from 'react-intl';

import styles from "resources/style/components/sidebarStyle.jsx";

class Detail extends React.Component {
  constructor(props) {
    super(props);
  }

  buildText(item){
    const { classes } = this.props;
    return (
      <TextField
        style={{marginBottom:0,color:"white"}}
       label={item[0]["@value"].label}
       type={"text"}
       formControlProps={{
         fullWidth: true
       }}
       disabled={true}
       value={item[0]["@value"].value["@value"]?new Date(parseInt(item[0]["@value"].value["@value"])).toLocaleString():item[0]["@value"].value }
       className={classes.textField, classes.textWith}
       InputLabelProps={{
         shrink: true,
       }}
       margin="normal" />
    )
  };

  render(){
    const { classes, open,nodeName } = this.props;
    const sDisplay = open? "block":"none";
    return (
      <div className={classes.detailInfoPaneWrapper} style={{display:sDisplay}}>
        <div style={{width:"100%",display:"flex",color:"white",fontWeight:"500",fontSize:"2rem",paddingTop:"8px"}}>
          <span style={{textAlign:"center",margin:"auto",fontWeight:"500"}}>{nodeName}</span>
        </div>
        <Grid container className={classes.addContainer}>
          {
           this.props.nodeData? Object.keys(this.props.nodeData).map((i,key)=>this.buildText(this.props.nodeData[i])):null
          }
        </Grid>
      </div>
    );
  }

}

const mapStateToProps = (state, ownProps) => {
  return {
    }
};

const mapDispatchToProps = (dispatch)=>{
    return {
    }
}

export default withStyles(styles)(connect(mapStateToProps,mapDispatchToProps)(Detail));
