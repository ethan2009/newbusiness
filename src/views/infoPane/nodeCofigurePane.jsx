import React from "react";
import { connect } from 'react-redux';
import ColorPicker from 'react-color-picker';
import ss from 'react-color-picker/index.css';
import { SketchPicker,MaterialPicker, CirclePicker } from 'react-color';
// @material-ui/core components
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardAvatar from "components/Card/CardAvatar.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import withStyles from "@material-ui/core/styles/withStyles";
import InputLabel from "@material-ui/core/InputLabel";
import Drawer from "@material-ui/core/Drawer";
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';

import Button from "components/CustomButtons/Button.jsx";

import { FormattedMessage } from 'react-intl';

import constant from "utils/constant";

import styles from "resources/style/components/sidebarStyle.jsx";

class Detail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      color: "",
      caption:"id",
      stroke:0,
      displayColor:false
    };
  }

  onConfirm(color){
    this.setState({
      displayColor: !this.state.displayColor
    });
    if(this.props.handleColorChange){
      const oData = {
        color: this.state.color,
        size: this.state.stroke,
        caption: this.state.caption
      }
      this.props.handleColorChange(oData);
      this.setState({
        color: ""
      });
    }
  }

  handleColorChoose(color){
    if(this.props.handleColorChange){
      const oData = {
        color: color.hex.toUpperCase(),
        size: this.state.stroke,
        caption: this.state.caption,
        openType:this.props.openType
      }
      this.props.handleColorChange(oData);
    }
    this.setState({
      color:color.hex.toUpperCase()
    })
  }
  handleStrokeChange(value,e){
    if(this.props.handleColorChange){
      const oData = {
        color: this.state.color,
        size: value,
        caption: this.state.caption,
        openType:this.props.openType
      }
      this.props.handleColorChange(oData);
    }
    this.setState({
     stroke: value
    })
  }

  handleCaptionChange(e){
    if(this.props.handleColorChange){
      const oData = {
        color: this.state.color,
        size: this.state.stroke,
        caption: e.target.value,
        openType:this.props.openType
      }
      this.props.handleColorChange(oData);
    }
    this.setState({
     caption: e.target.value
    })
  }

  componentWillReceiveProps(nextProps){
    const sLabel = nextProps.graphFullData.chooseLabel;
    const oNodePro = nextProps.graphFullData.graphData.nodePro;
    const oEdgePro = nextProps.graphFullData.graphData.edgePro;
    if(nextProps.openType === "node" && sLabel && sLabel !=="" && oNodePro[sLabel]){
      this.setState({
        color: oNodePro[sLabel].color,
        caption:oNodePro[sLabel].caption,
        stroke:oNodePro[sLabel].size
      })
    }else if(nextProps.openType === "edge" && sLabel && sLabel !=="" && oEdgePro[sLabel]){
      this.setState({
        color: oEdgePro[sLabel].color,
        caption:oEdgePro[sLabel].caption,
        stroke:oEdgePro[sLabel].size
      })
    }
  }

  render(){
    const { classes, open ,openType} = this.props;
    const sDisplay = open? "flex":"none";
    const nodeProperty = this.props.properties || [];
    const sLabel = this.props.graphFullData.chooseLabel;
    let sStrokeClass = openType === "node" ? "stroke": "strokeLine";
    //
    const properties = openType === "node"?nodeProperty:["display","hidden"];
    const atroke = openType === "node" ? constant.sStroke : constant.sLineStroke;
    return (
      <div className={classes.configurationPaneWrapper} style={{display:sDisplay,justifyContent:"space-between"}} onClick={this.props.onClick}>
         <div style={{height:"44px",display:"flex",width:"auto",minWidth:"400px"}}>
           <div style={{background:this.state.color}} className={classes.label}>
              <span className={classes.LabelContent}>{sLabel}</span>
           </div>
           <div style={{padding:"8px 0",height:"28px"}}>
              <CirclePicker onChangeComplete={this.handleColorChoose.bind(this)} color={this.state.color} colors={constant.colors} width={510} ></CirclePicker>
           </div>
         </div>
         <div style={{height:"44px",display:"flex",width:"auto"}}>
           <span className={classes.labelText}>Stroke:</span>
             {atroke.map((item,key)=>{
               const sShadow = this.state.stroke === item? "0 0 4px 4px": "0 0 2px rgba(156, 39, 176, 0.2)";
               return (<div className={classes[sStrokeClass]}
                            style={ openType === "node" ? {width:item*10 + 8,height:item*10 + 8,background:"#d3d8de",boxShadow:sShadow}: {width:item*4,height:14,background:"#d3d8de",boxShadow:sShadow}}
                            onClick={this.handleStrokeChange.bind(this,item)}></div>)
             })}
         </div>
         <div style={{height:"44px",display:"flex",width:"auto",paddingRight:"8PX"}}>
           <span className={classes.labelText}>Caption:</span>
           <Select
             value={this.state.caption}
             className={classes.formControl}
             onChange={this.handleCaptionChange.bind(this)}
           >
           {properties.map((item,key)=>{
             return (<MenuItem value={item}>{item}</MenuItem>)
           })}
           </Select>
         </div>

      </div>
    );
  }

}
// <div>
//   <FormControl variant="filled" className={classes.formControl}>
//    <InputLabel htmlFor="filled-age-native-simple">Stroke</InputLabel>
//    <Select
//      value={this.state.stroke}
//      onChange={this.handleStrokeChange.bind(this)}
//    >
//    {constant.sStroke.map((item,key)=>{
//      return (<MenuItem value={item}>{item}</MenuItem>)
//    })}
//    </Select>
//  </FormControl>
//  <FormControl variant="filled" className={classes.formControl}>
//     <InputLabel htmlFor="filled-age-native-simple">Caption</InputLabel>
//     <Select
//       value={this.state.caption}
//       onChange={this.handleCaptionChange.bind(this)}
//     >
//     {this.props.properties && this.props.properties.map((item,key)=>{
//       return (<MenuItem value={item}>{item}</MenuItem>)
//     })}
//     </Select>
//   </FormControl>
// </div>
// <div className={classes.formControlButton}>
//   <Button className={classes.button} color="blue" onClick={this.onConfirm.bind(this)}><FormattedMessage id='BTN_CONFIRM'/></Button>
//   <Button className={classes.button} color="blue" onClick={this.cancel.bind(this)}><FormattedMessage id='BTN_CANCEL'/></Button>
// </div>


const mapStateToProps = (state, ownProps) => {
  return {
    }
};

const mapDispatchToProps = (dispatch)=>{
    return {
    }
}

export default withStyles(styles)(connect(mapStateToProps,mapDispatchToProps)(Detail));
