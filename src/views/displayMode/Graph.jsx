import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import * as d3 from 'd3';
import { Row, Form } from 'antd';

import util from 'utils/util';
import constant from 'utils/constant';
// import { chartReq} from './actionCreator';
// import './Chart.less';

const WIDTH = window.innerWidth -  504;
const HEIGHT = window.innerHeight - 214;
const R = 35;

let simulation;
let count = 0 ;
let keepTransform = {};

class Chart extends Component {
  constructor(props, context) {
    super(props, context);
    this.markerStore = [];
    this.forceChart = this.forceChart.bind(this);
    this.init = false;
    this.state = {

    };
  }

  componentWillMount() {
    this.props.dispatch(push('/Chart'));
  }

  componentDidMount() {
    if(this.props.graphData)
      this.forceChart(this.props.graphData.nodes, this.props.graphData.edges, this.props.graphData.edgePro, this.props.graphData.nodePro,this.props.selectNode,this.props);
  }

  componentWillReceiveProps(nextProps){
    // this.print(this.props.data);
    // if(nextProps.showFull){
    this.svg.style('width', nextProps.newWidth ).style("height",nextProps.newHeight);
    // }else{
    //   this.svg.style('width', nextProps.newWidth -  504).style("height",nextProps.newHeight);
    // }

    if(!nextProps.graphData){
      this.refs['theChart'].innerHTML = '';
      return;
    }else{
      if( nextProps.graphQKey !== this.props.graphQKey || nextProps.selectGraph.doubleClickChaneg !== this.props.selectGraph.doubleClickChaneg){
        this.forceChart(nextProps.graphData.nodes, nextProps.graphData.edges, nextProps.graphData.edgePro, nextProps.graphData.nodePro,nextProps.selectNode,nextProps);
        return;
      }
      //
      // if(nextProps.selectGraph.selectLabel !== this.props.selectGraph.selectLabel){
      //   this.forceChart(nextProps.graphData.nodes, nextProps.graphData.edges, nextProps.graphData.edgePro, nextProps.graphData.nodePro);
      //   return;
      // }

    }
    if(nextProps.selectGraph.colorChnaged !== this.props.selectGraph.colorChnaged){
      if(nextProps.selectedType === "node"){
        this.updateNodeColor(nextProps);
      }else{
        this.updateEdgeColor(nextProps);
      }

    }

  }

  onClick(nodesCircle,node){
    nodesCircle.style('stroke-width', (nodeOfSelected) => { // nodeOfSelected：所有节点, node: 选中的节点
      if (nodeOfSelected.id === node.id) { // 被点击的节点变色
        return 10;
      } else {
        return 2;
      }
    })
    .style('stroke-dasharray', (nodeOfSelected) => { // nodeOfSelected：所有节点, node: 选中的节点
      if (nodeOfSelected.id === node.id) { // 被点击的节点变色
        return 0;
      }
    });

    count += 1;
    setTimeout(() => {
      if (count === 1 && this.props.handleNodeClick) {
        this.props.handleNodeClick(this.props.graphQKey,node);
      } else if (count === 2) {
        this.props.handleNodeDoubleClick(this.props.graphQKey,node);
      }
      count = 0;
    }, 200);


    console.log(node)
  }

  updateNodeColor(nextProps){
    const nodePro = nextProps.graphData.nodePro;
    const edgePro = nextProps.graphData.edgePro;
    if(this.nodesCircle)
    this.nodesCircle.style('fill', (d)=>{
      return nodePro[d.name] && nodePro[d.name].color || "#fff";
    }).attr('r', (d)=>{
      return nodePro[d.name] && (nodePro[d.name].size * R ) || R;
    });

    this.nodesCircle.style('stroke-width', (nodeOfSelected) => { // nodeOfSelected：所有节点, node: 选中的节点
      if (nodeOfSelected.id === nextProps.selectNode.id) { // 被点击的节点变色
        return 10;
      } else {
        return 2;
      }
    })
    .style('stroke-dasharray', (nodeOfSelected) => { // nodeOfSelected：所有节点, node: 选中的节点
      if (nodeOfSelected.id === nextProps.selectNode.id) { // 被点击的节点变色
        return 0;
      }
    });

    for( let i = 0;i < this.markerStore.length; i++){
      let size = edgePro[this.markerStore[i].edgeName] && edgePro[this.markerStore[i].edgeName].size || 1;
      let color = edgePro[this.markerStore[i].edgeName] && edgePro[this.markerStore[i].edgeName].color;
      let nsize = nodePro[this.markerStore[i].nodeName] && nodePro[this.markerStore[i].nodeName].size || 1;
      this.markerStore[i].marker.selectAll("path").style('fill',edgePro[this.markerStore[i].edgeName] && edgePro[this.markerStore[i].edgeName].color || "#000");
      this.markerStore[i].marker.attr('d', 'M1,1 L9,5 L1,9 L5,5 L1,1');
      // this.markerStore[i].marker.attr('refX',  R * nsize/size/2  +4.5);
      // .attr('markerWidth', 20*size) // viewport
      // .attr('markerHeight', 20*size);
    }

    if(this.edgesLine)
    this.edgesLine.attr('d', (d)=>this.buildEdgePath(d,edgePro,nodePro,nextProps.graphData.edges));

    // this.edgesText.attr('dx', (d)=>{
    //   let ntsize = nodePro[d.target.name].size || 1;
    //   return ntsize* R + R;
    // })
    // .attr('dy', -3)
    // .attr('xlink:href', (d, i) => {
    //    return i && '#edgepath' +this.props.graphId + i;
    //  }) // 文字布置在对应id的连线上
    this.edgesText.selectAll('textPath')
    .style('pointer-events', 'none')
    .text((d) => {
      return edgePro[d.tag].caption==="display" ? d && d.tag : null;
    });

    this.nodesTexts
    .style('fill', (d)=>nodePro[d.name] && nodePro[d.name].color && constant.defaultColors[nodePro[d.name].color].textColorInt || "#fff")
    .style('stroke', (d)=>nodePro[d.name] && nodePro[d.name].color && constant.defaultColors[nodePro[d.name].color].borderColor || "#000")
    .text((d) => {
      let caption = nodePro[d.name] && nodePro[d.name].caption;
      let sValue = "";
      if(caption !== "id" && d.nodeData[caption] && d.nodeData[caption][0]){
        sValue = util.getValue(d.nodeData[caption][0]);
        //sValue = d.nodeData[caption][0]["@value"].value["@value"]?new Date(parseInt(d.nodeData[caption][0]["@value"].value["@value"])).toLocaleString():d.nodeData[caption][0]["@value"].value ;
      }
      return caption !== "id" ? sValue ||  d.id : d && d.id;
    });


  }

  buildEdgePath(d,edgePro,nodePro,edges,bText){
    let path = "";
    let size = edgePro[d.tag].size || 1;
    let nsize = nodePro[d.target.name].size || 1;
    let nssize = nodePro[d.source.name].size || 1;
    let aDedge = edges.filter((item)=>{
      let sourceId = item.source.id || item.source;
      let targetId = item.target.id || item.target;
      return sourceId === d.target.id && targetId === d.source.id;
    });
    let oTarget;
    let oSource;
    if(aDedge.length === 0){
      oTarget = this.getNewTarget(d.source.x,d.source.y,d.target.x,d.target.y,nsize,size);
      oSource = this.getNewTarget(d.target.x,d.target.y,d.source.x,d.source.y,nssize,0);
      path = !bText ? 'M ' + oSource.x + ' ' + oSource.y + ' L ' + oTarget.x + ' ' + oTarget.y : 'M ' + oTarget.x + ' ' + oTarget.y + ' L ' + oSource.x + ' ' + oSource.y;
    }else{
      oTarget = this.getNewTarget(d.source.x,d.source.y,d.target.x,d.target.y,nsize,size,true);
      let dx = d.target.x - d.source.x,//增量
          dy = d.target.y - d.source.y,
          dr = Math.sqrt(dx * dx + dy * dy);
      path = !bText ? "M" + d.source.x + ","
            + d.source.y + "A" + dr + ","
            + dr + " 0 0,1 " + oTarget.x + ","
            + oTarget.y :
            "M" + oTarget.x + ","
                  + oTarget.y + "A" + dr + ","
                  + dr + " 0 0,0 " + d.source.x + ","
                  + d.source.y;

    }
    return path;
  }

  updateEdgeColor(nextProps){
    const edgePro = nextProps.graphData.edgePro;
    const nodePro = nextProps.graphData.nodePro;
    if(this.edgesLine)
    this.edgesLine.style('stroke', (d)=>{
      return edgePro[d.tag] && edgePro[d.tag].color || "#fff";
    }).style('stroke-width', (d)=>{
      return edgePro[d.tag] && (edgePro[d.tag].size * 2 ) || 1;
    }).attr('d', (d)=>this.buildEdgePath(d,edgePro,nodePro,nextProps.graphData.edges));

    // this.edgesText.text((d) => { return edgePro[d.tag].caption==="display" ? d && d.tag : ""; });
    for( let i = 0;i < this.markerStore.length; i++){
      let size = edgePro[this.markerStore[i].edgeName] && edgePro[this.markerStore[i].edgeName].size || 1;
      let color = edgePro[this.markerStore[i].edgeName] && edgePro[this.markerStore[i].edgeName].color;
      let nsize = nodePro[this.markerStore[i].nodeName] && nodePro[this.markerStore[i].nodeName].size || 1;
      this.markerStore[i].marker.selectAll("path").style('fill',edgePro[this.markerStore[i].edgeName] && edgePro[this.markerStore[i].edgeName].color || "#000");
      this.markerStore[i].marker.attr('markerWidth', size >= 8 ? 3 : 4)
      .attr('markerHeight',  size >= 8 ? 3 : 4);
    }

    // this.edgesText.attr('dx', (d)=>{
    //   let nsize = nodePro[d.target.name].size || 1;
    //   return nsize*R + R;
    // })
    // .attr('dy', 5)
    // // .attr('filter',(d)=>{
    // //   if(!document.getElementById('filter'+this.props.graphId+d.id.relationId)){
    // //     const defs = this.g.append('defs');
    // //     const filters = defs.append('filter')
    // //     .attr('id', 'filter'+this.props.graphId+d.id.relationId)
    // //     .attr('drop-shadow','-25px 25px 25px #ffffff')
    // //     .attr('x', 0)
    // //     .attr('y', 0)
    // //     .attr('width', 1)
    // //     .attr('height', 1);
    // //     filters.append('feFlood').attr('flood-color','white');
    // //     filters.append('feComposite').attr('in','SourceGraphic');
    // //   }
    // //   return 'url(#filter'+this.props.graphId+d.id.relationId+')';
    // // })
    // .attr('xlink:href', (d, i) => {
    //    return i && '#edgepath' +this.props.graphId + i;
    //  }) // 文字布置在对应id的连线上
    this.edgesText.selectAll('textPath')
    .style('pointer-events', 'none')
    .text((d) => {
      return edgePro[d.tag].caption==="display" ? d && d.tag : null;
    });

    // this.nodesTexts.text((d) => {
    //   let caption = nodePro[d.name] && nodePro[d.name].caption;
    //   return caption ? d.nodeData[caption] ||  d.id : d && d.id;
    // });


  }

  getNewTarget(x1,y1,x2,y2,nodeSize,lineSize,bCurve){
    const sL = Math.sqrt(Math.pow(x2-x1,2) + Math.pow(y2-y1,2));
    let m = lineSize >= 8 ? 1.7 : 2.2;
    m = bCurve ? 1.3 : m;
    const k = (sL - R*nodeSize - Math.sqrt(2)*m*lineSize) / sL;

    let x3 = (x2-x1)*k + x1;
    let y3 = (y1-y2)*(1-k) + y2;
    return {x:x3,y:y3};
  }

  // func
  forceChart(aNodes, aEdges, aEdgePro, aNodePro,selectNode,nextProps) {
    let edgePro = {...aEdgePro};
    let nodePro = {...aNodePro};
    let nodes = aNodes.map((item,key)=>{return {...item}});
    let edges = aEdges.map((item,key)=>{return {...item}});
    this.markerStore = [];
    delete edgePro.All;
    delete nodePro.All;

    const that = this;
    this.refs['theChart'].innerHTML = '';


    function onDragStart(d) {
      // console.log('start');
      // console.log(d3.event.active);
      if (!d3.event.active) {
        simulation.alphaTarget(0.1) // 设置衰减系数，对节点位置移动过程的模拟，数值越高移动越快，数值范围[0，1]
          .restart();  // 拖拽节点后，重新启动模拟
      }
      d.fx = d.x;    // d.x是当前位置，d.fx是静止时位置
      d.fy = d.y;
    }
    function dragging(d) {
      d.fx = d3.event.x;
      d.fy = d3.event.y;
    }
    function onDragEnd(d) {
      if (!d3.event.active) simulation.alphaTarget(0);
      d.fx = d3.event.x;
      d.fy = d3.event.y;
    }
    const drag = d3.drag()
      .on('start', onDragStart)
      .on('drag', dragging) // 拖拽过程
      .on('end', onDragEnd);

    function onZoomStart(d) {
      // console.log('start zoom');
    }

    function zooming(d) {
      // 缩放和拖拽整个g
      // console.log('zoom ing', d3.event.transform, d3.zoomTransform(this));
      g.attr('transform', d3.event.transform); // 获取g的缩放系数和平移的坐标值。
    }
    function onZoomEnd() {
      // console.log('zoom end');
    }
    const zoom = d3.zoom()
    // .translateExtent([[0, 0], [WIDTH, HEIGHT]]) // 设置或获取平移区间, 默认为[[-∞, -∞], [+∞, +∞]]
      .scaleExtent([1 / 100, 10]) // 设置最大缩放比例
      .on('start', onZoomStart)
      .on('zoom', zooming)
      .on('end', onZoomEnd);

    let centerX = selectNode.fx ? selectNode.fx : WIDTH / 2;
    let centerY = selectNode.fy ? selectNode.fy : HEIGHT / 2;

    const simulation = d3.forceSimulation(nodes) // 指定被引用的nodes数组
    .force('link', d3.forceLink(edges).id(d => d.id).distance(d=>edgePro[d.tag] && edgePro[d.tag].distance || 200))
    .force('collision', d3.forceCollide().radius(()=>R))
    .force('center', d3.forceCenter(nextProps.newWidth / 2, nextProps.newHeight / 2))
    .force('charge', d3.forceManyBody().strength(-1500).distanceMax(400));

    // simulation.alphaDecay(0.1)
    // .alpha(1)
    // .restart();
    const svg = d3.select('#'+this.props.graphId).append('svg') // 在id为‘theChart’的标签内创建svg
    .style('width', nextProps.newWidth)
    .style('height', nextProps.newHeight)
    .on('click', () => {
      console.log('click', d3.event.target.tagName);
    })
    .call(zoom).on('dblclick.zoom', null).call(drag); // 缩放
    this.svg = svg;

    const g = svg.append('g'); // 则svg中创建g
    this.g = g;
    const edgesLine = svg.select('g')
    .selectAll('line')
    .data(edges) // 绑定数据
    .enter() // 添加数据到选择集edgepath
    .append('path') // 生成折线
    .attr('d', (d)=>this.buildEdgePath(d,edgePro,nodePro,edges)) // 遍历所有数据，d表示当前遍历到的数据，返回绘制的贝塞尔曲线
    .attr('id', (d, i) => { return 'edgepath' +this.props.graphId+ d.id.relationId; }) // 设置id，用于连线文字
    // .attr('marker-mid',(d)=>{
    //   const defs = g.append('defs');
    //   const arrowheads = defs.append('marker')
    //   .attr('id', 'rect'+this.props.graphId+d.id.relationId)
    //   .attr('markerUnits', 'userSpaceOnUse')
    //   .attr('markerWidth', "20px")
    //   .attr('markerHeight', (d.tag.length + 2)* 20 +"px")
    //   .attr('viewBox', '0 0 90 90')
    //   .attr('refX',  45)
    //   .attr('refY', 60)
    //   .attr('orient', 'auto');
    //   arrowheads.append('path')
    //   .attr('id', 'rectpath'+this.props.graphId+d.id.relationId)
    //   .attr('d', 'M0,30 L90,30 L90,90 L0,90 L0,30')
    //   .style('fill',"white");
    //   return 'url(#rect'+this.props.graphId+d.id.relationId+')';
    // })
    .attr('marker-end', function(d){
      let size = edgePro[d.tag].size || 1;
      let nsize = nodePro[d.target.name].size || 1;
      // .attr('markerWidth', 20*size) // viewport
      let iR = R * nodePro[d.target.name].size;
      const defs = g.append('defs'); // defs定义可重复使用的元素
      const arrowheads = defs.append('marker') // 创建箭头
      .attr('id', 'arrow'+this.props.graphId+d.id.relationId)
      .attr('markerUnits', 'strokeWidth') // 设置为strokeWidth箭头会随着线的粗细进行缩放
      // .attr('markerUnits', 'userSpaceOnUse') // 设置为userSpaceOnUse箭头不受连接元素的影响
      .attr('class', 'arrowhead')
      .attr('markerWidth', size >= 8 ? 3 : 4) // viewport
      .attr('markerHeight', size >= 8 ? 3 : 4) // viewport
      .attr('viewBox', '0 0 10 10') // viewBox
      // .attr('refX',  iR/2+4.5) // 偏离圆心距离
      .attr('refX',  4)
      .attr('refY', 4) // 偏离圆心距离
      .attr('orient', 'auto'); // 绘制方向，可设定为：auto（自动确认方向）和 角度值
      arrowheads.append('path')
      .attr('d', 'M0,0 L8,4 L0,8 L4,4 L0,0') // d: 路径描述，贝塞尔曲线
      .style('fill',edgePro[d.tag] && edgePro[d.tag].color || "#000"); // 填充颜色


      this.markerStore.push({id:d.id.relationId,nodeName:d.target.name,edgeName:d.tag,marker:arrowheads});

      return 'url(#arrow'+this.props.graphId+d.id.relationId+')';
    }.bind(this)) // 根据箭头标记的id号标记箭头
    .style("fill","none")
    .style('stroke', (d)=>edgePro[d.tag] && edgePro[d.tag].color || "#000") // 颜色
    .style('stroke-width', (d)=>edgePro[d.tag] && edgePro[d.tag].size * 2 || 1); // 粗细

    const edgereversal = svg.select('g')
    .selectAll('line')
    .data(edges)
    .enter()
    .append('path')
    .attr('d', (d)=>'M ' + d.target.x + ' ' + d.target.y + ' L ' + d.source.x + ' ' + d.source.y)
    .attr('id', (d, i) => { return 'edgepath_reversal' +this.props.graphId+ d.id.relationId; })
    .style("fill","none")
    .style("stroke","none");

    this.edgesLine = edgesLine;

    const nodesCircle = svg.select('g')
    .selectAll('circle')
    .data(nodes)
    .enter()
    .append('circle') // 创建圆
    .attr('r', (node)=>{
      let iR = R * nodePro[node.name].size;
      return iR;
    }); // 半径

    nodesCircle.style('fill', (d)=>nodePro[d.name] && nodePro[d.name].color || "#fff")
    .style('stroke', '#000000') // 边框颜色
    // .style('stroke-width', 2) // 边框粗细
    .style("cursor","pointer")
    .style('stroke-opacity','0.25')
    .on('click', this.onClick.bind(this,nodesCircle))
    .call(drag); // 拖拽单个节点带动整个图

    nodesCircle.style('stroke-width', (nodeOfSelected) => { // nodeOfSelected：所有节点, node: 选中的节点
      if (nodeOfSelected.id === selectNode.id) { // 被点击的节点变色
        return 10;
      } else {
        return 2;
      }
    })
    .style('stroke-dasharray', (nodeOfSelected) => { // nodeOfSelected：所有节点, node: 选中的节点
      if (nodeOfSelected.id === selectNode.id) { // 被点击的节点变色
        return 0;
      }
    })

    this.nodesCircle = nodesCircle;

    const nodesTexts = svg.select('g')
    .selectAll('text')
    .data(nodes)
    .enter()
    .append('text')
    .attr('dy', '.3em') // 偏移量
    .attr('text-anchor', 'middle') // 节点名称放在圆圈中间位置
    .style('fill', (d)=>nodePro[d.name] && nodePro[d.name].color && constant.defaultColors[nodePro[d.name].color].textColorInt || "#fff")
    .style('font-size', '1rem') // 颜色
    .style('font-weight', '300') // 颜色
    .style('stroke', (d)=>nodePro[d.name] && nodePro[d.name].color && constant.defaultColors[nodePro[d.name].color].borderColor || "#000")
    .style('stroke-width', 2) // 边框粗细
    .style('stroke-opacity','0.25')
    .style('pointer-events', 'none') // 禁止鼠标事件
    .text((d) => { // 文字内容
      let caption = nodePro[d.name] && nodePro[d.name].caption;
      let sValue = "";
      if(caption !== "id" && d.nodeData[caption] && d.nodeData[caption][0]){
        sValue = util.getValue(d.nodeData[caption][0]);
        //sValue = d.nodeData[caption][0]["@value"].value["@value"]?new Date(parseInt(d.nodeData[caption][0]["@value"].value["@value"])).toLocaleString():d.nodeData[caption][0]["@value"].value ;
      }
      return caption !== "id" ? sValue ||  d.id : d && d.id;
    });

    this.nodesTexts = nodesTexts;
    // .attr("tspan",((d)=>d.name))
    // .attr("tspan",((d)=>d.id))

    const edgesText = svg.select('g').selectAll('.edgelabel')
    .data(edges)
    .enter()
    .append('text') // 为每一条连线创建文字区域
    .attr('class', 'edgelabel')
    // .attr('dx', (d)=>{
    //   let nsize = nodePro[d.target.name].size || 1;
    //   return nsize*R + R;
    // })
    .attr('text-anchor',"middle")
    .attr('dy', 5);
    // .attr('filter',(d)=>{
    //   if(!document.getElementById('filter'+this.props.graphId+d.id.relationId)){
    //     const defs = this.g.append('defs');
    //     const filters = defs.append('filter')
    //     .attr('id', 'filter'+this.props.graphId+d.id.relationId)
    //     .attr('drop-shadow','-25px 25px 25px #ffffff')
    //     .attr('x', 0)
    //     .attr('y', 0)
    //     .attr('width', 1)
    //     .attr('height', 1);
    //     filters.append('feFlood').attr('flood-color','red');
    //     filters.append('feComposite').attr('in','SourceGraphic');
    //   }
    //   return 'url(#filter'+this.props.graphId+d.id.relationId+')';
    // });

    edgesText.append('textPath')// 设置文字内容
    .attr('xlink:href', (d, i) => {
      if(d.source.x < d.target.x){
        return '#edgepath' +this.props.graphId+ d.id.relationId;
      }else{
        return '#edgepath_reversal' +this.props.graphId+ d.id.relationId;
      }
    })
    .style('pointer-events', 'none')
    .text((d) => { return d && d.tag; })
    .style('fill', '#000000')
    .style('font-size', '1rem')
    .style('font-weight', '500')
    // .style('text-shadow', '0 0 20px #ffffff')
    .style('stroke', '#ffffff')
    .style('stroke-width', 2)
    .attr("startOffset","50%")
    .style('stroke-opacity','0.5');

    this.edgesText = edgesText;

    nodesCircle.append('title')
    .text((node) => { // .text设置气泡提示内容
      return node.name;
    });

    edgesLine.append('title')
    .text((node) => { // .text设置气泡提示内容
      return node.tag;
    });


    simulation.on('tick', () => {
      // 更新节点坐标
      // nodesCircle.attr('transform', (d) => {
      //   return d && 'translate(' + d.x + ',' + d.y + ')';
      // });
      nodesCircle.attr("cx", function (d) { return d.x; })
          .attr("cy", function (d) { return d.y; });
      //
      // nodesTexts.attr("cx", function (d) { return d.x; })
      //     .attr("cy", function (d) { return d.y; });
      // 更新节点文字坐标
      nodesTexts.attr('transform', (d) => {
        return 'translate(' + (d.x) + ',' + d.y + ')';
      });

      edgesLine.attr('d',(d)=>this.buildEdgePath(d,edgePro,nodePro,edges));

      edgereversal.attr('d', (d)=>this.buildEdgePath(d,edgePro,nodePro,edges,true));

      edgesText
      .selectAll('textPath')
      .attr('xlink:href', (d, i) => {
        if(d.source.x < d.target.x){
          return '#edgepath' +this.props.graphId+ d.id.relationId;
        }else{
          return '#edgepath_reversal' +this.props.graphId+ d.id.relationId;
        }
      });
      // edgesLine.attr('d', (d) => {
      //   let centerX1 = (d.source.x + d.target.x)/4;
      //   let centerX2 = (d.source.y + d.target.y)/2;
      //   let path = "";
      //   let size = edgePro[d.tag].size || 1;
      //   let nsize = nodePro[d.target.name].size || 1;
      //   let aDedge = edges.filter((item)=>item.source === d.target && item.target === d.source);
      //   var oTarget = this.getNewTarget(d.source.x,d.source.y,d.target.x,d.target.y,nsize,size);
      //   if(aDedge.length === 0){
      //     path = 'M ' + d.source.x + ' ' + d.source.y + ' L ' + oTarget.x + ' ' + oTarget.y;
      //   }else{
      //
      //   let dx = d.target.x - d.source.x,//增量
      //       dy = d.target.y - d.source.y,
      //       dr = Math.sqrt(dx * dx + dy * dy);
      //   path = "M" + d.source.x + ","
      //         + d.source.y + "A" + dr + ","
      //         + dr + " 0 0,1 " + oTarget.x + ","
      //         + oTarget.y;
      //     //path = 'M ' + d.source.x + ' ' + d.source.y + ' C '+ d.target.x+' '+d.source.y + ", "+ d.target.x+" "+d.target.y +", " + d.target.x + ' ' + d.target.y;
      //     // let markerPath = "M0,0 L8,4 L0,8 L4,4 L0,0";
      //     // for( let i = 0;i < this.markerStore.length; i++){
      //     //   if(d.id.relationId === this.markerStore[i].id ){
      //     //     // this.markerStore[i].marker.attr('refY', 1);
      //     //     this.markerStore[i].marker.selectAll("path").attr('d', markerPath);
      //     //     break;
      //     //   }
      //     // }
      //   }
      //   return path;
      // });
      // 更新连线文字位置
      // edgesText.attr('transform', (d, i) => {
      //   return 'rotate(0)';
      // });
    });

  }

  render() {
    // this.print(this.props.data);
    return (
      <Row style={{ minWidth: 100 }}>
        <div className="outerDiv">
          <div className="theChart" id={this.props.graphId} ref="theChart">

          </div>
        </div>
      </Row>
    );
  }
}

Chart.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {

  };
}

const WrappedChart = Form.create({})(Chart);
export default connect(mapStateToProps)(WrappedChart);
