import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import util from 'utils/util';

import withStyles from "@material-ui/core/styles/withStyles";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import Icon from '@material-ui/core/Icon';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import { FormattedMessage } from 'react-intl';
// core components
import tableStyle from "resources/style/components/tableStyle";

class TableDis extends Component {
  constructor(props, context) {
    super(props, context);
  }

  formatDate(str){
    return str["@value"]?new Date(parseInt(str["@value"])).toLocaleString():str.toString();
  }

  render(){
    const { classes, selectedNodeProperty, graphData, nodePro,labelName } = this.props;
    return (
      <Table className={classes.table}>
        <TableHead style={{background: nodePro && nodePro.color }}>
          <div>
            <span style={{color: nodePro[labelName] && nodePro[labelName].color,fontWeight:"500",textShadow:"0 0 2px #000000",fontSize:"1.5rem" }}>{labelName}</span>
          </div>
          <TableRow style={{background:nodePro[labelName]?nodePro[labelName].color:"#fff"}}>
            {selectedNodeProperty && selectedNodeProperty.map((prop, key) => {
              return (
                <TableCell
                  style={{color:"#fff",textShadow:"0 0 2px #000000"}}
                  className={classes.tableCell + " " + classes.tableHeadCell}
                  key={key} >
                  {prop}
                </TableCell>
              )
            })}
          </TableRow>
        </TableHead>
        <TableBody>
          {graphData && graphData.nodes && graphData.nodes.map((prop, key) => {
            return labelName === prop.name ?(
              <TableRow className={classes.tableRow}>
                {selectedNodeProperty && selectedNodeProperty.map((p, k) => {
                  return (
                    <TableCell
                      key={k} >
                      <div className={classes.tableCell + " " + classes.tableHeadCell}>
                        <span className={classes.tableCellText}>
                          {
                            p === "id" ? prop.id.toString(): (prop.nodeData[p] ? util.getValue(prop.nodeData[p][0]) :"")
                          }
                        </span>
                      </div>
                    </TableCell>
                  );
                })}
              </TableRow>
            ): null
          })}

        </TableBody>
      </Table>
    )
  }
}

export default withStyles(tableStyle)(TableDis);
