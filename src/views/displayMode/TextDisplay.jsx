import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

import withStyles from "@material-ui/core/styles/withStyles";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import Icon from '@material-ui/core/Icon';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import CircularProgress from '@material-ui/core/CircularProgress';
import { FormattedMessage } from 'react-intl';
// core components
import tableStyle from "resources/style/components/tableStyle";

class TextDisplay extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      busy:true
    }
  }

  componentDidMount() {
    const { graphData} = this.props;
    new Promise((r)=>{

      setTimeout(()=>{
        this.refs['jsonPrint'].innerHTML = this.syntaxHighlight({nodes:graphData.nodes,edges:graphData.edges});
        r();
      },500)

    }).then(()=>{
      this.setState({busy:false});
    })
  }

  syntaxHighlight(json) {
    const { classes, graphData} = this.props;
    if (typeof json != 'string') {
        json = JSON.stringify(json, undefined, 2);
    }
    json = json.replace(/&/g, '&').replace(/</g, '<').replace(/>/g, '>');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function(match) {
        var cls = 'darkorange';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = '#FF6666';
            } else {
                cls = '#00cc00';
            }
        } else if (/true|false/.test(match)) {
            cls = 'blue';
        } else if (/null/.test(match)) {
            cls = 'magenta';
        }
        return '<span style="color:'+cls+'">' + match + '</span>';
    });
  }

  render(){
    const { classes, graphData} = this.props;
    return (
      <div style={{width:"100%",height:"100%",overflow:"hidden"}}>
        <pre className={classes.jsonPrint} ref="jsonPrint">
        </pre>
        {
          this.state.busy === true?(
            <div id="busy" className={classes.busyContent}>
              <CircularProgress className={classes.progress} size={150} />
            </div>
          ): null
        }
      </div>
    )
  }
}

export default withStyles(tableStyle)(TextDisplay);
