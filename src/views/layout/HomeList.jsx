/* eslint-disable */
import React from "react";
import { connect } from 'react-redux';
import PropTypes from "prop-types";
import { FormattedMessage } from 'react-intl';
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import IconButton from "@material-ui/core/IconButton";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Icon from '@material-ui/core/Icon';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Menu from "@material-ui/icons/Menu";
import DialogDel from "components/deleteDlg/DialogDel.jsx";

import HomeListStyle from "resources/style/layouts/HomeList.jsx";


class HomeList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      setSelecter: false,
      deleteOpen:false
    };
  }

  addSelect(){
    if(this.props.itemSelect){
      this.props.itemSelect()
    }
  }

  itemSelect(item,e){
    if(this.props.itemSelect){
      this.props.itemSelect(item)
    }
  }

  deleteGraph(item,e){
    if(this.props.deleteGraph){
      this.props.deleteGraph(this.item)
    }
    this.setState({
      deleteOpen: false
    })
  }

  cancelDelete(){
    this.setState({
      deleteOpen: false
    })
  }

  openDeleteDlg(item,e){
    e.stopPropagation()
    this.item = item;
    this.setState({
      deleteOpen: true
    });
  }

  render() {
    const { classes, ...rest } = this.props;
    return (
      <div className={classes.ManuList}>
        <List component="nav"  className={classes.ManuListGH}>
          {this.props.listData.map((item,key)=>{
            return (
              <ListItem
                button
                onClick={this.itemSelect.bind(this,item)}
                selected={item.selected}
                className={!item.selected? classes.Item :classes.itemSelected}
              >
                <ListItemIcon className={classes.Button}>
                  <Icon>all_inclusive</Icon>
                </ListItemIcon>
                <div onClick={this.openDeleteDlg.bind(this,item)}><Icon>cancel</Icon></div>
                <span className={classes.Text} >{item.name}</span>
              </ListItem>
            )
          })}
          <ListItem
            button
            onClick={this.addSelect.bind(this)}
            selected={this.props.addSelect}
            className={!this.props.addSelect? classes.Item:classes.itemSelected}
          >
            <ListItemIcon className={classes.Button}>
              <Icon>add_circle_outline</Icon>
            </ListItemIcon>
            <span className={classes.Text} >Add Graph</span>
          </ListItem>
        </List>
        <List component="nav" className={classes.ManuListSetup}>
          <ListItem
            button
            className={classes.Item}
          >
          <ListItemIcon className={classes.Button}>
            <Icon>settings</Icon>
          </ListItemIcon>
          </ListItem>
        </List>
        <DialogDel
          confirmDelete={this.deleteGraph.bind(this)}
          cancelDelete={this.cancelDelete.bind(this)}
          text={"TEXT_CONFIRM_DELETE_GRAPH"}
          open={this.state.deleteOpen}></DialogDel>
      </div>
    );
  }
}

HomeList.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(HomeListStyle)(HomeList);
