import React from "react";
import { connect } from 'react-redux';

// @material-ui/core components
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardAvatar from "components/Card/CardAvatar.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import withStyles from "@material-ui/core/styles/withStyles";
import InputLabel from "@material-ui/core/InputLabel";
import Drawer from "@material-ui/core/Drawer";
import Paper from '@material-ui/core/Paper';
import Icon from '@material-ui/core/Icon';
import InputAdornment from '@material-ui/core/InputAdornment';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

import Button from '@material-ui/core/Button';
import Graph from "views/displayMode/Graph.jsx";
import Table from "views/displayMode/Table.jsx";
import TextDisplay from "views/displayMode/TextDisplay.jsx";
import DetailInfo from 'views/infoPane/detailInfo';
import NodeCofigurePane from 'views/infoPane/nodeCofigurePane';

import constant from 'utils/constant';
import { FormattedMessage } from 'react-intl';

import styles from "resources/style/layouts/mainPane.jsx";
import generalAc from "actions/generalAction.js";

class FunctionArea extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      displayMode: 'G',//L->table, T->text
      queryStr:"",
      showFullScreen:false,
      selectNodeL: "",
      selectEdgeL:"",
      openType:""
    };
  }

  changDisplayMode(sMode){
    if(this.props.changDisplayMode)
    this.props.changDisplayMode(this.props.graphFullData.key,sMode);
  }

  onNodeLabelClick(node){
    this.labelClick = true;
    this.setState({
      selectNodeL:node,
      openType:"node"
    })
    if(this.props.handleNodeLabelClick)
      this.props.handleNodeLabelClick(this.props.graphFullData.key,node,this.props.displayMode === "G")
  }

  NodeCofigurePaneClick(){
    this.labelClick = true;
  }

  onEdgeLabelClick(edge){
    this.labelClick = true;
    this.setState({
      selectEdgeL:edge,
      openType:"edge"
    })
    if(this.props.handleEdgeLabelClick)
      this.props.handleEdgeLabelClick(this.props.graphFullData.key,edge,this.props.displayMode === "G")
  }

  handleQueryPaneCollapset(){
    if(this.props.handleQueryPaneCollapset)
      this.props.handleQueryPaneCollapset(this.props.graphFullData.key)
  }

  handleQueryPaneCloss(){
    if(this.props.handleQueryPaneCloss)
      this.props.handleQueryPaneCloss(this.props.graphFullData.key)
  }

  handleQueryPaneFullScreen(){
    if(this.props.handleQueryPaneFullScreen)
      this.props.handleQueryPaneFullScreen(!this.state.showFullScreen)
    this.setState({
      showFullScreen:!this.state.showFullScreen
    })
  }

  handleQueryPaneRestore(){
    if(this.props.handleQueryPaneRestore)
      this.props.handleQueryPaneRestore(this.props.graphFullData.key)
  }

  handleCloseDetail(){
    setTimeout(() => {
      if(this.labelClick){
        this.labelClick = false;
        return;
      }

      if(this.props.handleNFCancel)
        this.props.handleNFCancel(this.props.graphFullData.key)

      if(this.nodeClick){
        this.nodeClick  = false;
        return;
      }
      if(this.props.handleCloseDetail)
        this.props.handleCloseDetail(this.props.graphFullData.key)
    }, 200);
  }

  handleNodeClick(key,node){
    this.nodeClick = true;
    if(this.props.handleNodeClick){}
      this.props.handleNodeClick(key,node)
  }

  onQstrClick(str){
    if(this.props.handleQstrClick)
      this.props.handleQstrClick(str)
  }

  // handleNFCancel(){
  //   if(this.props.handleNFCancel)
  //     this.props.handleNFCancel(this.props.graphFullData.key)
  // }

  handleColorChange(oData){
    this.labelClick = true;
    if(this.props.handleColorChange)
      this.props.handleColorChange(this.props.graphFullData.key,oData)
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.displayMode !== "L"){
      this.setState({
        selectNodeL:""
      })
    }
  }


  renderBody(showEdge){
    const sWidth = this.state.showFullScreen?this.props.newWidth-114: (!this.props.showFull?this.props.newWidth-504:this.props.newWidth-244);
    const sHeight = this.state.showFullScreen?this.props.newHeight-134: (showEdge?this.props.newHeight-214:this.props.newHeight-173);
    if(this.props.graphFullData.graphData.nodes.length === 0){
      return null;
    }
    switch (this.props.displayMode) {
      case "L":
        return (
          <Table
            graphData={this.props.graphFullData.graphData}
            nodePro={this.props.graphFullData.graphData.nodePro}
            labelName={this.state.selectNodeL}
            selectedNodeProperty={this.props.graphFullData.nodeTypeProperties && this.props.graphFullData.nodeTypeProperties[this.state.selectNodeL]}></Table>
        )
      case "T":
        return (
          <TextDisplay
            graphData={this.props.graphFullData.graphData}
            ></TextDisplay>
        )
      case "G":
      default:
      return (
        <Graph handleNodeClick={this.handleNodeClick.bind(this)}
          showFull={this.props.showFull}
          newWidth={sWidth}
          selectedType={this.state.openType}
          newHeight={sHeight}
          graphId={"theChart"+this.props.keyNo}
          graphQKey={this.props.graphFullData.key}
          handleNodeDoubleClick={this.props.handleNodeDoubleClick}
          graphData={this.props.graphFullData.graphData}
          selectNode={this.props.oSelectNode}
          selectGraph={this.props.graphFullData}></Graph>
      )

    }
  }
  render(){
    const { classes, graphFullData,oSelectGraph,width } = this.props;

    const bCollapset = graphFullData.showCollapse?"none":null;
    const bCollapsetHieght = graphFullData.showCollapse?"auto":null;
    const showEdge = this.props.graphFullData.graphData.edgePro && Object.keys(this.props.graphFullData.graphData.edgePro).length > 0 ? true: false;
    const oStyle = this.state.showFullScreen? {height:window.innerHeight-20,width:window.innerWidth-20,zIndex:"999" ,position:"absolute",top:"10px",left:"10px"}:{height:bCollapsetHieght||window.innerHeight - 100 + "px",width:width}

    const nodeLabels = (
      <CardBody className={classes.LabelBody} style={{height:showEdge?"50%":"100%"}}>
        {
          this.props.graphFullData.graphData.nodePro && Object.keys(this.props.graphFullData.graphData.nodePro).map((pro,key)=>{
          let sShadow = graphFullData.chooseLabel === pro ?  "0 0 4px 4px": "0 0 2px rgba(156, 39, 176, 0.2)";
          return <div className={classes.baseLabel} onClick={this.onNodeLabelClick.bind(this,pro)} style={{background:this.props.graphFullData.graphData.nodePro[pro].color,boxShadow:sShadow,color:constant.defaultColors[this.props.graphFullData.graphData.nodePro[pro].color].textColorInt}}>
                   <span className={classes.LabelContent}>{pro +'('+this.props.graphFullData.graphData.nodePro[pro].count+')'}</span>
                 </div>})
        }
      </CardBody>
    );

    const edgeLabels = (
      <CardBody className={classes.LabelBody}>
        {
          this.props.graphFullData.graphData.edgePro && Object.keys(this.props.graphFullData.graphData.edgePro).map((pro,key)=>{
          let sShadow = graphFullData.chooseLabel === pro ?  "0 0 4px 4px": "0 0 2px rgba(156, 39, 176, 0.2)";
          return <div className={classes.baseLabelEdge} onClick={this.onEdgeLabelClick.bind(this,pro)} style={{background:this.props.graphFullData.graphData.edgePro[pro].color,boxShadow:sShadow,color:constant.defaultColors[this.props.graphFullData.graphData.edgePro[pro].color].textColorInt}} >
                   <span className={classes.LabelContent}>{pro+'('+this.props.graphFullData.graphData.edgePro[pro].count+')'}</span>
                 </div>})
        }
      </CardBody>
    );

    const sHeight = this.state.showFullScreen?this.props.newHeight-134: (showEdge?this.props.newHeight-214:this.props.newHeight-173);

    return (
        <Card className={classes.cardLayoutB} style={oStyle} onClick={this.handleCloseDetail.bind(this)}>
          <CardBody>
            <card className={classes.cardHeaderInfoBar}>
              <div className={classes.cardHeaderInfoBarTil}>
                <span>gremlin></span>
                <div className={classes.cardHeaderInfoBarTil2} onClick={this.onQstrClick.bind(this,graphFullData.QStr)}>{graphFullData.QStr}
                  <div>{graphFullData.QStr}</div>
                </div>
              </div>
              <div className={classes.cardHeaderInfoBarBtnC}>
                <Button className={classes.cardHeaderInfoBarBtn}
                  onClick={this.handleQueryPaneFullScreen.bind(this)}>{!this.state.showFullScreen?(<Icon>fullscreen</Icon>) : (<Icon>fullscreen_exit</Icon>)}</Button>
                {
                  this.state.showFullScreen?null:(
                    <Button className={classes.cardHeaderInfoBarBtn}
                      onClick={this.handleQueryPaneCollapset.bind(this)}
                      >{graphFullData.showCollapse?(<Icon>unfold_more</Icon>):(<Icon>unfold_less</Icon>)}</Button>
                  )
                }
                <Button className={classes.cardHeaderInfoBarBtn}
                  onClick={this.handleQueryPaneRestore.bind(this)}><Icon>restore</Icon></Button>
                {this.state.showFullScreen ? null:(
                  <Button className={classes.cardHeaderInfoBarBtn} onClick={this.handleQueryPaneCloss.bind(this)}><Icon>close</Icon></Button>
                )}
              </div>
            </card>
            <card className={classes.cardLayoutBL} style={{display:bCollapset}}>
              <CardBody className={classes.ManuBody}>
                <List component="nav"  className={classes.ManuListGH}>
                  <ListItem
                    button
                    className={this.props.displayMode === "G" ? classes.itemSelect:classes.Item}
                    onClick={this.changDisplayMode.bind(this,"G")}
                  >
                    <ListItemIcon className={classes.Button}>
                      <Icon>all_inclusive</Icon>
                    </ListItemIcon>
                    <ListItemText className={classes.Text} primary="Graph" />
                  </ListItem>
                  <ListItem
                    button
                    className={this.props.displayMode === "L" ? classes.itemSelect:classes.Item}
                    onClick={this.changDisplayMode.bind(this,"L")}
                  >
                    <ListItemIcon className={classes.Button}>
                      <Icon>grid_on</Icon>
                    </ListItemIcon>
                    <ListItemText className={classes.Text} primary="Table" />
                  </ListItem>
                  <ListItem
                    button
                    className={this.props.displayMode === "T" ? classes.itemSelect:classes.Item}
                    onClick={this.changDisplayMode.bind(this,"T")}
                  >
                    <ListItemIcon className={classes.Button}>
                      <Icon>translate</Icon>
                    </ListItemIcon>
                    <ListItemText className={classes.Text} primary="Text" />
                  </ListItem>
                </List>
              </CardBody>
            </card>
            <card className={classes.cardLayoutBH} style={{display:bCollapset,height:showEdge?"80px":"40px"}}>
              {nodeLabels}
              {showEdge?edgeLabels:null}
            </card>
            <card className={classes.cardLayoutBC} style={{display:bCollapset,height: sHeight,top:showEdge?"114px":"74px"}}>
              {this.renderBody(showEdge)}
            </card>
            <DetailInfo
              open={graphFullData.showDetail}
              nodeData={this.props.oSelectNode.nodeData}
              nodeName={this.props.oSelectNode.name}
              handleCloseDetail={this.handleCloseDetail.bind(this)}>
            </DetailInfo>
            <NodeCofigurePane
              openType={this.state.openType}
              onClick={this.NodeCofigurePaneClick.bind(this)}
              open={graphFullData.showNodeCofigure}
              node={graphFullData.selectedNode}
              graphFullData={graphFullData}
              properties={graphFullData.nodeTypeProperties[graphFullData.chooseLabel]}
              handleColorChange={this.handleColorChange.bind(this)}>
            </NodeCofigurePane>
          </CardBody>
        </Card>
    );
  }

}

const mapStateToProps = (state, ownProps) => {
  return {
    }
};

const mapDispatchToProps = (dispatch)=>{
    return {
    }
}

export default withStyles(styles)(connect(mapStateToProps,mapDispatchToProps)(FunctionArea));
