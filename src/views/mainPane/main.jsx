import React from "react";
import { connect } from 'react-redux';
import RichTextEditor from 'react-rte';
// @material-ui/core components
import Card from "components/Card/Card.jsx";
import ColorInput from "components/customContorls/colorInput.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardAvatar from "components/Card/CardAvatar.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import withStyles from "@material-ui/core/styles/withStyles";
import InputLabel from "@material-ui/core/InputLabel";
import Drawer from "@material-ui/core/Drawer";
import Paper from '@material-ui/core/Paper';
import Icon from '@material-ui/core/Icon';
import InputAdornment from '@material-ui/core/InputAdornment';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

import Button from '@material-ui/core/Button';
import Graph from "views/displayMode/Graph.jsx";
import Table from "views/displayMode/Table.jsx";
import FunctionArea from "views/mainPane/functionArea.jsx";
import TextDisplay from "views/displayMode/TextDisplay.jsx";
import keywords from 'utils/keywords';

import { FormattedMessage } from 'react-intl';

import styles from "resources/style/layouts/mainPane.jsx";
import generalAc from "actions/generalAction.js";

class MainPane extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      queryStr:"",
      selectNodeL: ""
    };
  }

  clearSearchStr(){
    this.setState({
      queryStr:""
    })
  }

  handleQstrClick(str){
    this.setState({
      queryStr:str
    })
  }

  query(){
    if (this.props.nodeQuery) {
      this.props.nodeQuery(this.state.queryStr);
    }
  }

  searchStrChange(e){
    let sQueryStr = e.target.value;
    // sQueryStr = sQueryStr.replace(new RegExp('<div>', "g" ),"\n");
    // sQueryStr = sQueryStr.replace(new RegExp('</div>', "g" ),"");
    // let sQueryStr = e.target.value;
    this.setState({
      queryStr:  sQueryStr
    })
  }

  keyEnterDown(e){

    if (this.props.nodeQuery) {
      this.props.nodeQuery(this.state.queryStr);
    }
   // }else if(e.ctrlKey  && e.keyCode == 10){
   //   this.setState({
   //     queryStr:this.state.queryStr + "\n"
   //   })
   // }
  }

  // componentDidMount() {
  //   document.getElementById("search").addEventListener('keyup',this.keyEnterDown.bind(this));
  // }

  componentDidUpdate(){
    // let sTop = this.props.oSelectGraph.queryList ? this.props.oSelectGraph.queryList.length * (window.innerHeight - 100) : 0;
    // this.refs.fnContent.scrollTop = sTop;
  }

  componentWillReceiveProps(nextProps){
    if(this.props.oSelectGraph.queryList && nextProps.oSelectGraph.queryList && this.props.oSelectGraph.queryList.length === nextProps.oSelectGraph.queryList.length) return;
    this.refs.fnContent.scrollTop = 0;
  }


  render(){
    const { classes,oSelectGraph } = this.props;
    const sWidth = oSelectGraph.queryList && oSelectGraph.queryList.length > 1? "calc(100% + 8px)" :"100%" ;
    return (
      <div>
        <Card className={classes.cardLayoutH}>
          <CardBody className={classes.cardLayoutHBody}>
            <div className={classes.searchControl}>
              <div className={classes.searchInputAd}>
                <p className={classes.searchInputAdText}>gremlin></p>
              </div>
              <ColorInput
                value={this.state.queryStr}
                keyWords={keywords}
                onChange={this.searchStrChange.bind(this)}
                onEndInput={this.keyEnterDown.bind(this)}
                >
              </ColorInput>
            </div>
            <div className={classes.headerButtonC} >
              <Button variant="fab" className={classes.headerButton} onClick={this.query.bind(this)} mini={true}>
                <Icon>arrow_forward_ios</Icon>
              </Button>
              <Button variant="fab" className={classes.headerButton} onClick={this.clearSearchStr.bind(this)} mini={true}>
                  <Icon>clear</Icon>
              </Button>
            </div>
          </CardBody>
        </Card>
        <div className={classes.cardLayoutContent} ref="fnContent" style={{height:window.innerHeight - 90 + "px"}}>
          { oSelectGraph.queryList && oSelectGraph.queryList.map((item,key)=>{
              return (<FunctionArea
                showFull={this.props.showFull}
                newWidth={this.props.newWidth}
                newHeight={this.props.newHeight}
                keyNo={key}
                width={sWidth}
                displayMode={item.displayMode || "G"}
                graphFullData={item}
                handleQstrClick={this.handleQstrClick.bind(this)}
                oSelectNode={this.props.oSelectNode}
                handleQueryPaneCloss={this.props.handleQueryPaneCloss}
                handleQueryPaneRestore={this.props.handleQueryPaneRestore}
                handleQueryPaneFullScreen={this.props.handleQueryPaneFullScreen}
                handleNodeClick={this.props.handleNodeClick}
                handleColorChange={this.props.handleColorChange}
                handleNFCancel={this.props.handleNFCancel}
                changDisplayMode={this.props.changDisplayMode}
                handleCloseDetail={this.props.handleCloseDetail}
                handleNodeLabelClick={this.props.handleNodeLabelClick}
                handleEdgeLabelClick={this.props.handleEdgeLabelClick}
                handleNodeDoubleClick={this.props.handleNodeDoubleClick}
                handleQueryPaneCollapset={this.props.handleQueryPaneCollapset}
                oSelectGraph={this.props.oSelectGraph}></FunctionArea>)
            })}
        </div>
      </div>
    );
  }

}

const mapStateToProps = (state, ownProps) => {
  const oSelectGraph = state.general.graphList.filter(item=>item.selected)[0];
  return {
      oSelectGraph: oSelectGraph || {},
      oSelectNode:state.general.selectedNode
    }
};

const mapDispatchToProps = (dispatch)=>{
    return {
      nodeQuery:(str)=>dispatch(generalAc.nodeQuery(str)),
      handleNodeClick:(qKey,node)=>dispatch(generalAc.handleNodeClick(qKey,node)),
      handleNodeDoubleClick:(qKey,node)=>dispatch(generalAc.handleNodeDoubleClick(qKey,node)),
      handleColorChange:(qKey,oData)=>dispatch(generalAc.handleColorChange(qKey,oData)),
      handleNFCancel:(qKey)=>dispatch(generalAc.handleNFCancel(qKey)),
      handleCloseDetail:(qKey)=>dispatch(generalAc.handleCloseDetail(qKey)),
      handleNodeLabelClick:(qKey,name,bShowcf)=>dispatch(generalAc.handleNodeLabelClick(qKey,name,bShowcf)),
      handleEdgeLabelClick:(qKey,name,bShowcf)=>dispatch(generalAc.handleEdgeLabelClick(qKey,name,bShowcf)),
      handleQueryPaneCollapset:(qKey)=>dispatch(generalAc.handleQueryPaneCollapset(qKey)),
      handleQueryPaneCloss:(qKey)=>dispatch(generalAc.handleQueryPaneCloss(qKey)),
      handleQueryPaneRestore:(qKey)=>dispatch(generalAc.handleQueryPaneRestore(qKey)),
      changDisplayMode:(qKey,sMode)=>dispatch(generalAc.changDisplayMode(qKey,sMode))
    }
}

export default withStyles(styles)(connect(mapStateToProps,mapDispatchToProps)(MainPane));
