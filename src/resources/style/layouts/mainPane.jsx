const baseLabel={
  width:"auto",
  margin:"6px",
  borderRadius:"400px",
  padding:"2px",
  boxShadow: "0 0 2px rgba(156, 39, 176, 0.2)"
};

const baseLabelForRelation = {
    width: "auto",
    margin: "6px",
    borderRadius: "5px",
    padding: "2px",
    boxShadow: "0 0 2px rgba(156, 39, 176, 0.2)"
};
const baseHeight = window.innerHeight - 100 + "px";
const ContentbaseHeight = window.innerHeight - 80 + "px";
const ContentHeight = window.innerHeight - 214 + "px";

const appStyle = theme => ({
  cardLayoutH:{
    marginTop:"10px",
    height:"70px",
    width:"100%",
    marginBottom:"0px"
  },
  cardLayoutB:{
    marginTop:"0px",
    marginBottom:"10px",
    width:"100%",
    // height:`calc(100% - 100px)`//baseHeight
  },
  cardLayoutContent:{
    marginTop:"10px",
    paddingRight:"12px",
    "&::-webkit-scrollbar":{
      width: "8px !important"
    },
    "&::-webkit-scrollbar-track": {
      backgroundColor: "#EDEDED"
    },
    "&::-webkit-scrollbar-thumb": {
      borderRadius: "0.5rem",
      backgroundColor: "#5B5B5B"
    },
    overflow:"auto",
    // marginTop:"10px",
    width:"100%"
    // height:`calc(100% - 214px)`//ContentbaseHeight
  },
  cardHeaderInfoBar:{
    width:"100%",
    height:"31px",
    position:"absolute",
    top:0,
    left:0,
    display:"flex",
    borderBottom:"#E8E8E8 1px solid"
  },
  cardHeaderInfoBarBtn:{
    minHeight:"16px",
    height:"31px",
    padding:"0px",
    marginLeft:"8px",
    marginRight:"8px",
    textTransform:"none",
    minWidth:"36px"
  },
  cardHeaderInfoBarBtnC:{
    position:"absolute",
    right:"0px"
  },
  cardHeaderInfoBarTil:{
    textAlign: "left",
    fontSize: "1rem",
    lineHeight: "1.75rem",
    marginLeft: "1rem",
    width:`calc(100% - 220px)`,
    overflow:"hidden",
    textOverflow: "ellipsis",
    whiteSpace: "nowrap",
    display:"flex"
  },
  cardHeaderInfoBarTil2:{
    // textDecoration:"underline",
    overflow: "hidden",
    textOverflow:"ellipsis",
    whiteSpace: "nowrap",
    color:"blue",
    cursor:"pointer",
    "&:hover div":{
      position:"absolute",
      background:"#E8E8E8",
      color:"#5B5B5B",
      fontSize:"1rem",
      borderRadius:"5px",
      padding:"0 10px",
      zIndex:"99"
    }
  },
  cardLayoutBL:{
    margin:"0",
    padding:"0",
    width:"72px",
    height:`calc(100% - 32px)`,
    position:"absolute",
    top:"32px",
    left:"0",
    borderRight:"#E8E8E8 1px solid"
  },
  cardLayoutBH:{
    margin:"0",
    padding:"0",
    width:`calc(100% - 80px)`,
    height:"80px",
    position:"absolute",
    top:"32px",
    left:"80px"
  },
  cardLayoutBC:{
    margin:"0",
    padding:"0",
    width:`calc(100% - 80px)`,
    // height:`calc(100% - 80px)`,//ContentHeight,
    position:"absolute",
    top:"114px",
    left:"80px"
  },
  cardLayoutHBody:{
    padding:"0",
    display:"flex"
  },
  textField:{
    margin: "auto 16px auto 0",
    width: `calc(100% - 262px)`,
    padding: "0 0 0 16px",
    "& div":{
      alignItems:"start",
      "& p":{
        color:"blue",
        fontWeight:"500"
      },
      "& textarea":{
        fontStyle:"italic",
        background:"#E8E8E8 ",
        fontWeight:"500",
        color:"#5B5B5B",
        borderRadius:"5px"
      }
    }
  },
  headerButton:{
    margin:"auto 8px auto 16px"
  },
  headerButtonC:{
    position:"absolute",
    height:"100%",
    display:"flex",
    right:"0",
    width:"130px"
  },
  ManuBody:{
    padding:"0"
  },
  Item:{
    padding:"10px 0",
    display: "block",
    // background:"#BEBEBE",
    borderBottom:"#E8E8E8 1px solid"
  },
  itemSelect:{
    padding:"10px 0",
    display: "block",
    background:"#BEBEBE",
    borderBottom:"#E8E8E8 1px solid"
  },
  Button:{
    display: "flex",
    margin: "auto"
  },
  Text:{
    padding:"0",
    textAlign:"center",
    "& .MuiTypography-subheading":{
      fontSize:"0.5rem"
    }
  },
  LabelBody:{
    overflowX:"scroll",
    "&::-webkit-scrollbar":{
      height: "4px !important"
    },
    "&::-webkit-scrollbar-track": {
      backgroundColor: "#EDEDED"
    },
    "&::-webkit-scrollbar-thumb": {
      borderRadius: "0.5rem",
      backgroundColor: "#5B5B5B"
    },
    padding:"0",
    height:"50%",
    display:"flex",
    borderBottom:"#E8E8E8 1px solid"
  },
  ManuListGH:{
    padding:"0"
  },
  LabelContent:{
    fontSize:"0.75rem",
    lineHeight:"1rem",
    // color:"#ffffff",
    padding:"2px 4px",
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    // textShadow: "0 0 2px #000000",
    fontWeight: "500",
  },
  baseLabel:{
    display:"flex",
    cursor:"pointer",
    ...baseLabel
  },
  baseLabelEdge:{
    display:"flex",
    cursor:"pointer",
    ...baseLabelForRelation
  },
  searchInputAd:{
    alignItems:"start",
    marginRight:"4px",
    maxHeight:"2rem",
    display:"flex"
  },
  searchInputAdText:{
    color:"blue",
    fontWeight:"500",
    lineHeight:"1.46429em",
    margin:"0",
    display:"block",
    fontFamily:'"Roboto", "Helvetica", "Arial", sans-serif'
  },
  searchInput:{
    width:"100%",
    position:"relative",
    flexGrow:1,
    margin:0,
    border:0,
    fontSize:"1rem",
    verticalAlign:"middle",
    boxSizing:"content-box",
    height:"100%",
    display:"block",
    fontStyle:"italic",
    background:"#E8E8E8 ",
    fontWeight:"500",
    color:"#5B5B5B",
    lineHeight:"1.2rem",
    maxHeight:"57px",
    overflowY:"auto",
    borderRadius:"5px",
    padding:"0 4px",
    overflowX:"hidden",
  },
  searchControl:{
    margin: "auto 16px",
    width: `calc(100% - 262px)`,
    padding: "4px 0",
    display:"flex",
    // transition: "transform 200ms cubic-bezier(0.0, 0, 0.2, 1)0ms",
    transition:"transform 200mx cubic-bezier()",
    borderBottom:"solid 2px #303f9f"
    // borderBottom: "solid 2px #303f9f",
    // "& ::before":{
    //   left: 0,
    //   right: 0,
    //   bottom: 0,
    //   content: "\\00a0",
    //   position:"absolute",
    //   transition: "border-bottom-color 200ms cubic-bezier(0.4, 0, 0.2, 1) 0ms",
    //   borderBottom: "2px solid rgba(0, 0, 0, 0.87)",
    //   pointerEvents:"none"
    // },
    // "& ::after":{
    //   left: 0,
    //   right: 0,
    //   bottom: 0,
    //   content: "",
    //   position:"absolute",
    //   transform: "scaleX(0)",
    //   transition: "transform 200ms cubic-bezier(0.0, 0, 0.2, 1)0ms",
    //   borderBottom: "2px solid #303f9f",
    //   pointerEvents:"none"
    // }
  }
});
export default appStyle;
