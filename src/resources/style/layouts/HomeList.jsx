import {
  drawerWidth,
  transition,
  container
} from "resources/style/base.jsx";

const appStyle = theme => ({
  wrapper: {
    position: "relative",
    top: "0",
    height: "100vh"
  },
  showDraver:{
    position:"absolute",
    zIndex:"999"
  },
  // ButtonDelete:{
  //     position:"absolute",
  //     width:"24px",
  //     height:"24px",
  //     right:2,
  //     top:8,
  //     background:"#ffffff",
  //     // color:"red",
  //     fontSize:"4rem",
  //     lineHeight:"1.25rem",
  //     textAlign:"center",
  //     borderRadius:"100%"
  //   },
  ManuListSetup:{
    height:"48px",
    padding:"0",
    background:"#2F4F4F",
    borderTop:"#BEBEBE 1px solid"
  },
  ManuListGH:{
    height:`calc(100% - 48px)`,
    overflow: "auto",
    padding:"0"
  },
  ManuList:{
    background:"#5B5B5B",
    overflow: "hidden",
    position: "relative",
    float: "left",
    ...transition,
    maxHeight: "100%",
    width: "100px",
    height:"100%",
    overflowScrolling: "touch"
  },
  Item:{
    padding:"12px 0",
    display: "block",
    background:"#5B5B5B",
    borderTop: "#BEBEBE 1px solid",
    "& div":{
        position:"absolute",
        width:"24px",
        height:"24px",
        right:2,
        top:8,
        // background:"#ffffff",
        color:"#ff6666",
        fontSize:"4rem",
        lineHeight:"1.25rem",
        textAlign:"center",
        borderRadius:"100%",
        display:"none"
      },
    "&:hover div": {
        display:"flex"
    }
  },
  // showDelete:{
  //   display:"none"
  // },
  itemSelected:{
    padding:"12px 0",
    display: "block",
    color:"#ffffff",
    background:"#212121",
    borderTop: "#BEBEBE 1px solid",
    "& div":{
        position:"absolute",
        width:"24px",
        height:"24px",
        right:2,
        top:8,
        // background:"#ffffff",
        color:"#ff6666",
        fontSize:"4rem",
        lineHeight:"1.25rem",
        textAlign:"center",
        borderRadius:"100%",
        display:"none"
      },
    "&:hover div": {
        display:"flex"
    }
  },
  Button:{
    display: "flex",
    color:"#ffffff",
    margin: "auto"
  },
  ButtonSetup:{
    display: "flex",
    margin: "auto",
    height:"76px"
  },
  Text:{
    padding:"0",
    color:"#ffffff",
    textAlign:"center",
    display:"block"
  },
  mainPanel: {
    [theme.breakpoints.up("md")]: {
      width: `calc(100% - 360px)`
    },
    overflow: "auto",
    position: "relative",
    float: "right",
    ...transition,
    maxHeight: "100%",
    width: "100%",
    overflowScrolling: "touch"
  },
  mainPanelFull:{
    [theme.breakpoints.up("md")]: {
      width: `calc(100% - 100px)`
    },
    overflow: "auto",
    position: "relative",
    float: "right",
    ...transition,
    maxHeight: "100%",
    width: "100%",
    overflowScrolling: "touch"
  },
  content: {
    marginTop: "70px",
    padding: "30px 15px",
    minHeight: "calc(100vh - 123px)"
  },
  container,
  map: {
    marginTop: "70px"
  }
});

export default appStyle;
