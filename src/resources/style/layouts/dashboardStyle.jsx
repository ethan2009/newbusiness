import {
  drawerWidth,
  transition,
  container
} from "resources/style/base.jsx";

const appStyle = theme => ({
  wrapper: {
    overflow:"hidden",
    position: "relative",
    top: "0",
    height: "100vh"
  },
  showDraver:{
    position:"absolute",
    zIndex:"999"
  },
  ManuList:{
    overflow: "auto",
    position: "relative",
    float: "left",
    ...transition,
    maxHeight: "100%",
    width: "100px",
    height:"100%",
    overflowScrolling: "touch"
  },
  mainPanel: {
    overflow: "auto",
    position: "relative",
    transition:"all 0.33s cubic-bezier(0.685, 0.0473, 0.346, 1)",
    float: "right",
    ...transition,
    maxHeight: "100%",
    width: `calc(100% - 360px)`,
    overflowScrolling: "touch"
  },
  busyContent:{
    position:"absolute",
    height:"100%",
    width:"100%",
    opacity:"0.5",
    zIndex:"999",
    background:"#000000",
    display:"flex"
  },
  progress:{
    zIndex:"1000",
    color:"blue",
    margin:"auto"
  },
  mainPanelFull:{
    transition:"all 0.33s cubic-bezier(0.685, 0.0473, 0.346, 1)",
    overflow: "auto",
    position: "relative",
    float: "right",
    ...transition,
    maxHeight: "100%",
    width: `calc(100% - 100px)`,
    overflowScrolling: "touch"
  },
  mainClassFullScree:{
    position:"absolute",
    width:"100%",
    height:"100%",
    zIndex:"999"
  },
  content: {
    marginTop: "0px",
    padding: "0px 15px",
    minHeight: "calc(100vh - 123px)"
  },

  container,
  map: {
    marginTop: "0px"
  }
});

export default appStyle;
