import {
  primaryColor,
  dangerColor,
  successColor,
  defaultFont
} from "resources/style/base.jsx";

const customInputStyle = {
  disabled: {
    "&:before": {
      backgroundColor: "transparent !important"
    }
  },
  underline: {
    "&:hover:not($disabled):before,&:before": {
      borderColor: "#D2D2D2 !important",
      borderWidth: "1px !important"
    },
    "&:after": {
      borderColor: primaryColor
    }
  },
  underlineError: {
    "&:after": {
      borderColor: dangerColor
    }
  },
  underlineSuccess: {
    "&:after": {
      borderColor: successColor
    }
  },
  labelRoot: {
    ...defaultFont,
    color: "#AAAAAA !important",
    fontWeight: "400",
    fontSize: "14px",
    lineHeight: "1.42857"
  },
  labelRootError: {
    color: dangerColor
  },
  labelRootSuccess: {
    color: successColor
  },
  feedback: {
    position: "absolute",
    top: "18px",
    right: "0",
    zIndex: "2",
    display: "block",
    width: "24px",
    height: "24px",
    textAlign: "center",
    pointerEvents: "none"
  },
  marginTop: {
    marginTop: "16px"
  },
  formControl: {
    paddingBottom: "10px",
    margin: "27px 0 0 0",
    position: "relative"
  },
  searchInputDisplay:{
    width:`calc(100% - 8px)`,
    // width:"100%",
    position:"relative",
    flexGrow:1,
    margin:0,
    border:0,
    fontSize:"1rem",
    verticalAlign:"middle",
    boxSizing:"content-box",
    height:"100%",
    display:"block",
    fontStyle:"italic",
    background:"#E8E8E8 ",
    fontWeight:"500",
    color:"#5B5B5B",
    lineHeight:"1.2rem",
    maxHeight:"60px",
    overflowY:"auto",
    borderRadius:"5px",
    padding:"0 4px",
    overflowX:"hidden",
    "& div":{
      margin:0
    }
  },
  searchInput:{
    width:"100%",
    position:"relative",
    flexGrow:1,
    margin:0,
    border:0,
    fontSize:"1rem",
    verticalAlign:"middle",
    boxSizing:"content-box",
    height:"100%",
    zIndex:"2",
    display:"block",
    fontStyle:"italic",
    background:"rgba(255,255,255,0)",
    fontWeight:"500",
    color:"rgba(255,255,255,0)",
    caretColor:"black",
    lineHeight:"1.2rem",
    maxHeight:"60px",
    height:"60px",
    overflowY:"auto",
    borderRadius:"5px",
    // padding:"0 4px",
    overflowX:"hidden"
  },
  searchInputField:{
    width:`calc(100% - 8px)`,
    position:"relative",
    flexGrow:1,
    margin:0,
    border:0,
    fontSize:"1rem",
    verticalAlign:"middle",
    boxSizing:"content-box",
    height:"100%",
    zIndex:"2",
    display:"block",
    fontStyle:"italic",
    background:"rgba(255,255,255,0)",
    fontWeight:"500",
    color:"rgba(255,255,255,0)",
    caretColor:"black",
    lineHeight:"1.2rem",
    maxHeight:"60px",
    height:"60px",
    overflowY:"auto",
    borderRadius:"5px",
    padding:"0 4px",
    overflowX:"hidden",
    "& div":{
      margin:0
    }
  },
};

export default customInputStyle;
